<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/admin_style/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
	tinymce.init({
	    mode : "specific_textareas",
	    editor_selector : /(mceEditor|wysiwyg)/,
	    language : 'uk_UA',
	    theme: "modern",
	    plugins: [
	        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
	        "searchreplace wordcount visualblocks visualchars code fullscreen",
	        "insertdatetime media nonbreaking save table contextmenu directionality",
	        "emoticons template paste textcolor filemanager"
	    ],
	    //width:"100%",
	    height: 300,
	    //image_advtab: true,
   		//toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code",
   		
	    convert_urls : false, //запрезаем конвертацию ссылок
	    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	    toolbar2: "print preview media | forecolor backcolor emoticons",
	    image_advtab: true,
	    visual: true,
    	visualblocks_default_state: true, //plugins: "visualblocks",
	    templates: [
	        {title: 'Цитата', content: '<blockquote>Текст цитаты</blockquote>'},
	        {title: 'Списки стиль 1', content: '<ul class="styled-list style-1"><li>List style 1</li></ul>'},
	        {title: 'Списки стиль 2', content: '<ul class="styled-list style-2"><li>List style 2</li></ul>'},
	        {title: 'Списки стиль 3', content: '<ul class="styled-list style-3"><li>List style 3</li></ul>'},
	        {title: 'Списки стиль 4', content: '<ul class="styled-list style-4"><li>List style 4</li></ul>'},
	        {title: 'Списки стиль 5', content: '<ul class="styled-list style-5"><li>List style 5</li></ul>'},
	        {title: 'Списки стиль 6', content: '<ul class="styled-list style-6"><li>List style 6</li></ul>'},
	        {title: 'Блок текста в 2 ряда', content: '<div class="row"><div class="eight columns"><h3 class="title-block">1/2 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div><div class="eight columns"><h3 class="title-block">1/2 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div></div>'},
	        {title: 'Блок текста в 3 ряда', content: '<div class="row"><div class="one-third column"><h3 class="title-block">1/3 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris.</p></div><div class="one-third column"><h3 class="title-block">1/3 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris.</p></div><div class="one-third column"><h3 class="title-block">1/3 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris.</p></div></div>'},
	        {title: 'Блок текста в 4 ряда', content: '<div class="row"><div class="four columns"><h3 class="title-block">1/4 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div><div class="four columns"><h3 class="title-block">1/4 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div><div class="four columns"><h3 class="title-block">1/4 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div><div class="four columns"><h3 class="title-block">1/4 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div></div>'},
	        {title: 'Блок текста 1/3 и 2/3', content: '<div class="row"><div class="one-third column"><h3 class="title-block">1/3 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div><div class="two-thirds column"><h3 class="title-block">2/3 Column</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula ac mauris. Ut adipiscing, leo nec. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div></div>'},
	        {title: 'Параграф стиль 1', content: '<p><span class="dropcap1">P</span>ras aliquet. Integer faucibus, eros ac molestie placerat, enim tellus varius lacus, nec dictum nunc tortor id urna eros ac molestie placerat, enim tellus.</p>'},
	        {title: 'Параграф стиль 2', content: '<p><span class="dropcap2">P</span>ras aliquet. Integer faucibus, eros ac molestie placerat, enim tellus varius lacus, nec dictum nunc tortor id urna eros ac molestie placerat, enim tellus.</p>'},
	        {title: 'Параграф стиль 3', content: '<p><span class="dropcap3">P</span>ras aliquet. Integer faucibus, eros ac molestie placerat, enim tellus varius lacus, nec dictum nunc tortor id urna eros ac molestie placerat, enim tellus.</p>'},
	        {title: 'Параграф стиль 4', content: '<p><span class="dropcap4">P</span>ras aliquet. Integer faucibus, eros ac molestie placerat, enim tellus varius lacus, nec dictum nunc tortor id urna eros ac molestie placerat, enim tellus.</p>'},
	        {title: 'Ссылка - кнопка синяя', content: '<a class="button small blue" href="/">ТЕКСТ КНОПКИ</a>'},
	        {title: 'Ссылка - кнопка черная', content: '<a class="button small gray" href="/">ТЕКСТ КНОПКИ</a>'},
	        //{title: 'Аудио', content: ''},
	    ]
	});
	</script>
	
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    //'type'=>'inverse', // null or 'inverse'
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                //array('label'=>'Назад', 'url'=> Yii::app()->createUrl(''),'linkOptions'=>array('target'=>'_blank')),
                array('label'=>'Основные настройки', 'url'=>'#', 'visible'=>CBackend::isAdmin(9),
				        	'items' =>array(
                		array('label'=>'Пользователи', 'url'=>array('/backend/user/admin')),
                		array('label'=>'Текстовые стр.', 'url'=>array('/backend/textpages/admin')),
                		array('label'=>'Настройки', 'url'=>array('/backend/settings/admin')),
                		//array('label'=>'Слайдер', 'url'=>array('/backend/slider/admin')),
                		//array('label'=>'Баннеры', 'url'=>array('/backend/banners/admin')),
                	),
                ),
                array('label'=>'Основные Разделы', 'url'=>'#', 'visible'=>CBackend::isAdmin(9),
                	'items' =>array(
                	
                		//array('label'=>'Команда', 'url'=>array('/backend/team/admin'),),                			
		                array('label'=>'Новости/Анонсы', 'url'=>'/backend/articles/admin',
				        			'items' =>array(
				        				array('label'=>'Список новостей', 'url'=>array('/backend/articles/admin'),),
				        				array('label'=>'Авторы', 'url'=>array('/backend/authors/admin'),),
				        			),
						        ),
						        array('label'=>'Видео', 'url'=>array('/backend/video/admin'),
						        'items' =>array(
				        				array('label'=>'Все видео', 'url'=>array('/backend/video/admin'),),
				        				array('label'=>'Категории', 'url'=>array('/backend/videoCategory/admin'),),
				        			),
						        ),
						        /*array('label'=>'Продукты', 'url'=>'#',
				        			'items' =>array(
				        				array('label'=>'Продукты', 'url'=>array('/backend/products/admin'),),				        				
				        				array('label'=>'Где используется', 'url'=>array('/backend/productsUsed/admin'),),
				        				array('label'=>'Типы продуктов', 'url'=>array('/backend/productsTypes/admin'),),
				        			),
						        ),
						        array('label'=>'Вопрос-ответ', 'url'=>'#', 'visible'=>CBackend::isAdmin(9),
				        			'items' =>array(
				        				array('label'=>'Вопрос-ответ список', 'url'=>array('/backend/questions/admin'),),
				        				array('label'=>'Категории', 'url'=>array('/backend/questionsCategory/admin'),),
				        			),
						        ),*/
						        /*array('label'=>'Тесты', 'url'=>'#', 'visible'=>CBackend::isAdmin(9),
				        			'items' =>array(
				        				array('label'=>'Тест', 'url'=>array('/backend/tests/admin'),),
				        				array('label'=>'Тест - Результаты', 'url'=>array('/backend/testsResults/admin'),),
				        				array('label'=>'Тест продуктовый', 'url'=>array('/backend/testsProducts/admin'),),
				        				array('label'=>'Тест продуктовый - Результаты', 'url'=>array('/backend/testsProductsResults/admin'),),
				        			),
						        ),
						        array('label'=>'Семплинг', 'url'=>'#',
				        			'items' =>array(
				        				array('label'=>'Список пользователей', 'url'=>array('/backend/usersSampling/admin'),),				        		
				        				array('label'=>'Импорт csv', 'url'=>array('/backend/usersSampling/importCsv'),),				        		
				        				array('label'=>'Экспорт csv', 'url'=>array('/backend/usersSampling/exportCsv'),),				        		
				        			),
						        ),
						        array('label'=>'Gmap Карта', 'url'=>'#',
				        			'items' =>array(
				        				array('label'=>'Импорт', 'url'=>array('/backend/gmap/import'),),
				        				array('label'=>'Экспорт', 'url'=>array('/backend/gmap/export'),),
				        				array('label'=>'Область', 'url'=>array('/backend/gmapRegions/admin'),),
				        				array('label'=>'Город', 'url'=>array('/backend/gmapCities/admin'),),
				        				array('label'=>'Магазин', 'url'=>array('/backend/gmapShops/admin'),),
				        			),
						        ),
						        array('label'=>'Промо раздел', 'url'=>'#', 'visible'=>CBackend::isAdmin(9),
				        			'items' =>array(
				        				array('label'=>'Страницы', 'url'=>array('/backend/promoPages/admin'),),
				        				array('label'=>'Тексты', 'url'=>array('/backend/promoTexts/admin'),),
				        			),
						        ),*/
				        
                	)
                ) ,				        
								array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
								array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>
	
	
	<div class="row">
		<div class="span9">
			<div id='AjFlash' class="alert in alert-block fade alert-success" style="display: none;"></div>
			<?php 
			$this->widget('bootstrap.widgets.TbAlert', array(
	        'block'=>true, // display a larger alert block?
	        'fade'=>true, // use transitions?
	        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
	        'alerts'=>array( // configurations per alert type
	            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
	        ),
	    )); ?>
    </div>
	</div>	

	<div id="footer">
		<?php /*Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>*/?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
