<?php

class TextpagesController extends CFrontend
{

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($alias)
	{		
		$model = Textpages::model()->find('alias=:alias AND status=1', array('alias'=>$alias));
		if($model===null)
			throw new CHttpException(404,'Такой страницы не существует');
			
		//Meta
		Yii::app()->clientScript->registerMetaTag($model->{'hdesc'.DMultilangHelper::getPrefix()}, 'description');
		Yii::app()->clientScript->registerMetaTag($model->{'hkeyw'.DMultilangHelper::getPrefix()}, 'keywords');
		$this->pageTitle = $model->{'htitl'.DMultilangHelper::getPrefix()};		
		
		$view='view';		
		$pathView = Yii::getPathOfAlias('application.views.textpages.'.$alias).'.php';		 
		if (file_exists($pathView)){
			$view = $alias;
		}
		
		
		//contactsForm
		$modelContactsForm = new ContactsForm();
		
		if(@$_POST['ContactsForm']['name'] == $modelContactsForm->getAttributeLabel('name')){ $_POST['ContactsForm']['name']=false;}
		if(@$_POST['ContactsForm']['subject'] == $modelContactsForm->getAttributeLabel('subject')){ $_POST['ContactsForm']['subject']=false;}
		if(@$_POST['ContactsForm']['message'] == $modelContactsForm->getAttributeLabel('message')){ $_POST['ContactsForm']['message']=false;}
		if(isset($_POST['ajax']) && $_POST['ajax']==='contact-form')
	  {
	    echo CActiveForm::validate($modelContactsForm);
	    Yii::app()->end();
	  }
		
		//Ajax Validation
		if(isset($_POST['ContactsForm'])){
			$modelContactsForm->attributes=$_POST['ContactsForm'];			
			
	    if($modelContactsForm->validate()) {
	    	//Send mail
				$mailer = Yii::createComponent('application.extensions.mailer.EMailer');
				$message = 'Были заполнены такие поля как:<br>
				<b>'.$modelContactsForm->getAttributeLabel('name').':</b> '.$modelContactsForm->name.'<br>
				<b>'.$modelContactsForm->getAttributeLabel('email').':</b> '.$modelContactsForm->email.'<br>
				<b>'.$modelContactsForm->getAttributeLabel('subject').':</b> '.$modelContactsForm->subject.'<br>
				<b>'.$modelContactsForm->getAttributeLabel('message').':</b> '.$modelContactsForm->message.'<br>';
				$mailer->IsHTML(true);
				$mailer->From = $modelContactsForm->email;
				$mailer->AddAddress(Yii::app()->params['adminEmail']);
				//$mailer->AddAddress('mail2@example.com');
				$mailer->FromName = $modelContactsForm->email;
				$mailer->CharSet = 'UTF-8';
				$mailer->Subject = 'Сообщение с '.Yii::app()->name;
				$mailer->Body = $message;
				
				if($mailer->Send()){ 
			    echo CJSON::encode(array(
			      'status'=>'success'
			    ));
			    Yii::app()->end();
		  	}
		  	
    	} else {
		    	echo CActiveForm::validate($modelContactsForm);
		    	Yii::app()->end();
		  }   
  	}
		
		
		$this->render($view,array(
			'model'=>$model,
			'modelContactsForm'=>$modelContactsForm,
		));
	}
	

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Textpages::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
}
