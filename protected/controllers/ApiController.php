<?php

class ApiController extends CFrontend
{

    public function actionArticles()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 't.status=:active';
        $criteria->params = array('active' => 1);

        //If Search
        if (isset($_GET['searcharticlesname'])){
            $criteria->addCondition("t.hkeyw_ru LIKE :name OR t.name_ru LIKE :name OR t.name2_ru LIKE :name");//hkeyw_ru,name_ru,name2_ru
            $criteria->params = $criteria->params + array('name' => "%{$_GET['searcharticlesname']}%");
        }

        $criteria->order = 'timework desc';
        $dataProvider = new CActiveDataProvider('Articles', array(
            'criteria'=>$criteria,
            'countCriteria'=>$criteria,
            'pagination' => array(
                'pageSize'=>6,
            )
        ));



        $data = array(
            'totalItemCount' => (int)$dataProvider->getTotalItemCount(),
            'getItemCount' => (int)$dataProvider->getItemCount(),
        );


        foreach ($dataProvider->getData() as $item) {
            /** @var Articles $attributes */
            $attributes = $item->getAttributes();
            $attributes['timecreate'] = (int)$attributes['timecreate'];
            $attributes['timeupdate'] = (int)$attributes['timeupdate'];
            $attributes['timework'] = (int)$attributes['timework'];
            $attributes['image'] = $item->getPhotoNameUrl($item->id . '.jpg', $item->timeupdate);

            $data['items'][] = $attributes;
        }

        echo json_encode($data);
    }

    public function actionArticle($id = 0)
    {

        $condition = 't.status=:active AND t.id=:id';
        $params = array('active' => 1, 'id' => $id);

        $model = Articles::model()
            ->findByPk($id, $condition, $params);

        if (! $model) {
            throw new Exception('Error, wrong article ID');
        }


        $attributes = $model->getAttributes();
        $attributes['timecreate'] = (int)$attributes['timecreate'];
        $attributes['timeupdate'] = (int)$attributes['timeupdate'];
        $attributes['timework'] = (int)$attributes['timework'];
        $attributes['image'] = $model->getPhotoNameUrl($model->id . '.jpg', $model->timeupdate);


        $photosModel = new Photos('search');
        $photosModel->unsetAttributes();
        $photoList = $photosModel->findAll('type="article" AND id_parent='.$model->id.' AND status=1 ORDER BY sort ASC');

        $photoCount = 0;
        foreach ($photoList as $photoModel) {
            $attributes['gallery'][$photoCount] = $photoModel->getAttributes();
            $attributes['gallery'][$photoCount]['img'] = Photos::model()->getPhotoUrl($photoModel->id);

            $photoCount++;
        }

        echo json_encode($attributes);
    }

    public function actionVideo()
    {
        $video = Video::model()->findAll('status = 1 ORDER BY sort asc');

        foreach ($video as $item) {
            $data['items'][] = $item->getAttributes();
        }

        echo json_encode($data);
    }

    public function actionStreamLink()
    {
        $settings = Settings::model()
            ->findByPk(9);

        echo $settings->value;
    }
}
