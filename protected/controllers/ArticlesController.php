<?php

class ArticlesController extends CFrontend
{
	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($alias=false)
	{
		//$model = Articles::model()->find('alias=:alias AND status=1', array('alias'=>$alias));		
		$model = Articles::model()->find('alias=:alias', array('alias'=>$alias));		
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		
		//Add views
		$connection=Yii::app()->db;
		$sql="UPDATE {$model->tableName()} set views=views+1 WHERE id=".(int)$model->id."";
		if($command=$connection->createCommand($sql)->execute()){
			$model->views++; 	
		}
		
			
		//$otherArticles = Articles::model()->findAll('id!=:id AND status=1', array('id'=>$model->id));		
		//Meta
		Yii::app()->clientScript->registerMetaTag($model->{'hdesc'.DMultilangHelper::getPrefix()}, 'description');
		Yii::app()->clientScript->registerMetaTag($model->{'hkeyw'.DMultilangHelper::getPrefix()}, 'keywords');
		$this->pageTitle = $model->{'htitl'.DMultilangHelper::getPrefix()};	
		
		$photosModel = new Photos('search');	
		$photosModel->unsetAttributes();
		
		$this->render('view',array(
			'model'=>$model,
			//'otherArticles'=>$otherArticles,
			'photoList'=>$photosModel->findAll('type="article" AND id_parent='.$model->id.' AND status=1 ORDER BY sort ASC'),
		));
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.status=:active';
		$criteria->params = array('active'=>1);
		
		//If Search
		if(isset($_GET['searcharticlesname'])){
			$criteria->addCondition("t.hkeyw_ru LIKE :name OR t.name_ru LIKE :name OR t.name2_ru LIKE :name");//hkeyw_ru,name_ru,name2_ru
			$criteria->params = $criteria->params + array('name'=>"%{$_GET['searcharticlesname']}%");
		}
		
		$criteria->order = 'timework desc';
		$dataProvider=new CActiveDataProvider('Articles', array(
		    'criteria'=>$criteria,
		    'countCriteria'=>$criteria,
		    'pagination'=>array(
		    	'pageSize'=>6,
		    )
		  ));
		
		//$dataProvider=new CActiveDataProvider('Articles');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Articles::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}
