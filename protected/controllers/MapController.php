<?php

class MapController extends CFrontend
{
	
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionGetCities(){
		header("Content-type: application/json");
		if((int)$_POST['id']){
			$cities = GmapCities::model()->findAll('region=:id', array(':id'=>$_POST['id']));
			$citiesFirst = current($cities);
			$shops = GmapShops::model()->findAll('city=:id and lat!=0 and lng!=0', array(':id'=>$citiesFirst->id));
			$response = array("success"=>true, "cities"=>array(), "shops"=>array());
			$response['cityfirst'] = array('id'=>$citiesFirst->id,'title'=>$citiesFirst->{'city'.DMultilangHelper::getPrefix()});
			foreach($cities as $row){
				array_push($response['cities'], array("id"=>$row->id, "title"=>$row->{'city'.DMultilangHelper::getPrefix()}));
			}
			foreach($shops as $row){
				array_push($response['shops'], array("id"=>$row->id, "adress"=>$row->{'adress'.DMultilangHelper::getPrefix()}, "description"=>$row->{'description'.DMultilangHelper::getPrefix()}, "lat"=>$row->lat, "lng"=>$row->lng));
			}
			$response['descriptions'] = array_unique(CHtml::listData($response['shops'], 'id','description'));
		}
		else{
			$response = array("error"=>true, "code"=>0);
		}
		echo json_encode($response);
	}
	
	public function actionGetShops(){
		header("Content-type: application/json");
		if((int)$_POST['id']){
			$response = array("success"=>true, "shops"=>array());
			$shops = GmapShops::model()->findAll('city=:id and lat!=0 and lng!=0', array(':id'=>$_POST['id']));
			foreach($shops as $row){
				array_push($response['shops'], array("id"=>$row->id, "adress"=>$row->{'adress'.DMultilangHelper::getPrefix()}, "description"=>$row->{'description'.DMultilangHelper::getPrefix()}, "lat"=>$row->lat, "lng"=>$row->lng));
			}
			$response['descriptions'] = array_unique(CHtml::listData($response['shops'], 'id','description'));
		} else if(@$_POST['name']){
			$response = array("success"=>true, "shops"=>array());
			$shops = GmapShops::model()->findAll('description'.DMultilangHelper::getPrefix().'=:name and lat!=0 and lng!=0', array(':name'=>$_POST['name']));
			foreach($shops as $row){
				array_push($response['shops'], array("id"=>$row->id, "adress"=>$row->{'adress'.DMultilangHelper::getPrefix()}, "description"=>$row->{'description'.DMultilangHelper::getPrefix()}, "lat"=>$row->lat, "lng"=>$row->lng));
			}
			$response['descriptions'] = array_unique(CHtml::listData($response['shops'], 'id','description'));
		}
		else{
			$response = array("error"=>true, "code"=>0);
		}
		echo json_encode($response);
	}
	
}