<?php

class IndexController extends CFrontend
{
	
	
	public function actionIndex()
	{
		$model = Textpages::model()->find('alias=:alias', array('alias'=>'main'));
			
		//Meta
		Yii::app()->clientScript->registerMetaTag($model->{'hdesc'.DMultilangHelper::getPrefix()}, 'description');
		Yii::app()->clientScript->registerMetaTag($model->{'hkeyw'.DMultilangHelper::getPrefix()}, 'keywords');
		$this->pageTitle = $model->{'htitl'.DMultilangHelper::getPrefix()};		
		
		$this->render('index');
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
}