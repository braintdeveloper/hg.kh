<?php

class ProductsController extends CFrontend
{
	 public function actions(){
     /*return array(
      	'captcha'=>array(
      		'class'=>'CCaptchaAction',
      	),
     );*/
     return array(
       'captcha'=>array(
         'class'=>'CaptchaExtendedAction',
         // if needed, modify settings
         'mode'=>CaptchaExtendedAction::MODE_MATH,
       ),
     );
   }
	
	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($alias=false)
	{
		$model = Products::model()->find('alias=:alias', array('alias'=>$alias));		
		if($model===null)
			throw new CHttpException(404,Yii::t('Text', 'The requested page does not exist.'));
			
		//Meta
		Yii::app()->clientScript->registerMetaTag($model->{'hdesc'.DMultilangHelper::getPrefix()}, 'description');
		Yii::app()->clientScript->registerMetaTag($model->{'hkeyw'.DMultilangHelper::getPrefix()}, 'keywords');
		$this->pageTitle = $model->{'htitl'.DMultilangHelper::getPrefix()};	
		
		//otherProducts
		$criteria = new CDbCriteria;
		$criteria->condition = 't.status=:active AND id!=:id';
		$criteria->params = array('active'=>1, 'id'=>$model->id);	
		$otherProducts = Products::model()->findAll($criteria);
			
		$this->render('view',array(
			'model'=>$model,
			'otherProducts'=>$otherProducts,
		));
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{		
		//Meta
		$meta = Textpages::model()->find('alias="products"');
		if($meta){
			Yii::app()->clientScript->registerMetaTag($meta->{'hdesc'.DMultilangHelper::getPrefix()}, 'description');
			Yii::app()->clientScript->registerMetaTag($meta->{'hkeyw'.DMultilangHelper::getPrefix()}, 'keywords');
			$this->pageTitle = $meta->{'htitl'.DMultilangHelper::getPrefix()};	
		}
		
		$criteria = new CDbCriteria;
		$criteria->condition = 't.status=:active';
		$criteria->params = array('active'=>1);
		$criteria->order = 'sort asc';
		$dataProvider=new CActiveDataProvider('Products', array(
		    'criteria'=>$criteria,
		    /*'countCriteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>2,
		    )*/
		  ));
		
		//$dataProvider=new CActiveDataProvider('Articles');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'meta'=>$meta,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Articles::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function actionSampling(){
		//Meta
		$meta = Textpages::model()->find('alias="sampling"');
		if($meta){
			Yii::app()->clientScript->registerMetaTag($meta->{'hdesc'.DMultilangHelper::getPrefix()}, 'description');
			Yii::app()->clientScript->registerMetaTag($meta->{'hkeyw'.DMultilangHelper::getPrefix()}, 'keywords');
			$this->pageTitle = $meta->{'htitl'.DMultilangHelper::getPrefix()};	
		}
		
		$model = new UsersSampling('addSampling');
		
		//if
		//header("Content-type: application/json");
		// if it is ajax validation request
		if(isset($_POST['UsersSampling'])){
			header("Content-type: application/json");
			$model->attributes=$_POST['UsersSampling'];
			$model->setAttribute('status_delivery',1);
			$model->setAttribute('phone',@$_POST['UsersSampling']['phonecode'].@$_POST['UsersSampling']['phonecode2']);
			
			if($model->save()){
				$response['success'] = 'ok';
				
				//Send Email
				$settings = CHtml::listData(Settings::model()->findAll(),'name','value');
				if($settings['samplingadminemail']){
					$mailer = Yii::createComponent('ext.mailer.EMailer');
					
					$message = $this->renderPartial('/index/samlingEmailTemplate', null, true);
					$mailer->IsHTML(true);
					$mailer->From = $settings['samplingadminemail'];
					$mailer->AddAddress($model->email);
					$mailer->FromName = $settings['sitename'];
					$mailer->CharSet = 'UTF-8';
					$mailer->Subject = 'Безкоштовний зразок';
					$mailer->Body = $message;
					$mailer->Send();
				}
				
			} else {			
			$response['form'] = $this->renderPartial('sampling',array(
				'model'=>$model,
				'meta'=>$meta,
				), true);
			}
			echo json_encode($response);
			Yii::app()->end();
		}
		
		$this->render('sampling',array(
			'model'=>$model,
			'meta'=>$meta,
		));
	}

}
