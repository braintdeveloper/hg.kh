<?php
class CDates
{

	public static function getMounth($number) {
		$months = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
		return $months[$number-1];
	}
	
	public static function getMounthMini($number) {
		$months = array('Янв', 'Фев', 'Мрт', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Нбр', 'Дек');
		return $months[$number-1];
	}

	public static function getMounthByTime($time) {
		return date('d',$time).' '.getMounth(date('n',$time)).' '.date('Y',$time);
	}

	public static function getDay($number) {
		$days = array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');
		return $mounths[$number-1];
	}
	
	public static function getDates($time,$type) {
		$date='';
		if($type == 'news') {
			$date = date('d.m.Y',$time);;
		} elseif($type == 'service') {
			$date = date('d.m.Y',$time);;
		}
		return $date;
	}
	
	/*
	* Youtube parser
	* return $link
	*/ 
	public function youtubelinkParser($link){
		// parse Youtube link, variable $v has link
		$video_url = parse_url($link);
		if(isset($video_url['query'])) {
			parse_str($video_url['query'], $parseVideo);
		}
		if(isset($parseVideo['v']) && $parseVideo['v'])
			$code = $parseVideo['v'];
		else
			$code = $link;			
		return $code;
	}
	
	/*
	* To unix Date
	* return $int
	*/
	public function toUnixDate($inputDate) {		
		$patern = '/^([0-9]{4})\/([0-9]{2})\/([0-9]{2})/';
		preg_match($patern,$inputDate,$matches);
		$output = '';
		if(!empty($matches)) {
			$output = mktime(0,0,0,$matches[2],$matches[3],$matches[1]);
		}
		return $output;
	}
	public function toEditorDate($inputDate) {		
		if(empty($inputDate))
			return '';
		$output = date('Y/m/d',$inputDate);
		return $output;
	}	

	public function toUnixDateLarge($inputDate) {
		$patern = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})\s([0-9]{2}):([0-9]{2})/';
		preg_match($patern,$inputDate,$matches);
		$output = '';
		if(!empty($matches)) {
			$output = mktime($matches[4],$matches[5],0,$matches[1],$matches[2],$matches[3]);
		}
		return $output;
	}	
	public function toEditorDateLarge($inputDate) {		
		if(empty($inputDate))
			return false;
		$output = date('m/d/Y H:i',$inputDate);
		return $output;
	}
	
	
	
	public static function getViewsText($int) {
        if (substr($int,-2,1)==1 && strlen($int)>1) { //10-19
            $text='Просмотров';
        } else if(substr($int,-1)==1){ //1
            $text='Просмотр';
        } else if(substr($int,-1)>=2 && substr($int,-1)<=4){ //2-4
            $text='Просмотра';
        } else { //0, 5-9
            $text='Просмотров';
        }

        return $commentText = $int.' '.$text;;
    }
	
	
}