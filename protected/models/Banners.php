<?php

/**
 * This is the model class for table "banners".
 *
 * The followings are the available columns in table 'banners':
 * @property string $id
 * @property string $name
 * @property string $link
 * @property string $timecreate
 * @property string $timeupdate
 */
class Banners extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'banners';
	}
	
	public $image_uk;
	public $image_ru;
	 
 	public function behaviors(){
    return array(
      'imageBehavior' => array(
        'class' => 'ImageBehavior',
        'imagePath' => '/images/banners/',
        'imageField' => 'image',
        'imageFolderName' => 'banners',
      ),
    );
  }
  
  public function beforeSave(){
		if(parent::beforeSave())
    {
			if($this->isNewRecord){
				$this->setAttribute('timecreate', TIME);
				$this->setAttribute('timeupdate', TIME);
			} else{
				$this->setAttribute('timeupdate', TIME);
			}
			
			return true;
    }
    else
        return false;
	}
	
	public function afterSave(){
		parent::afterSave();
	  
	  //Upload UK picture
    $this->image_uk=CUploadedFile::getInstance($this,'image_uk');
		if($this->image_uk){
			$imagePath = $this->getPhotoNamePath($this->id.'.uk.jpg');
			$this->image_uk->saveAs($imagePath);
		}
		//Upload RU picture
    $this->image_ru=CUploadedFile::getInstance($this,'image_ru');
		if($this->image_ru){
			$imagePath = $this->getPhotoNamePath($this->id.'.ru.jpg');
			$this->image_ru->saveAs($imagePath);
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_uk, link', 'required'),
			array('name_uk, name_ru link', 'length', 'max'=>255),
			array('timecreate, timeupdate', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_uk, name_ru link, timecreate, timeupdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_uk' => 'Name Uk',
			'name_ru' => 'Name Ru',
			'link' => 'Link',
			'timecreate' => 'Timecreate',
			'timeupdate' => 'Timeupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name_uk',$this->name_uk,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('timecreate',$this->timecreate,true);
		$criteria->compare('timeupdate',$this->timeupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Banners the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
