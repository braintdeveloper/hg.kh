<?php
	class ContactsForm extends CFormModel
	{
		public $name;
		public $email;
		public $subject;
		public $message;
		public $isSend = false;

		/**
		* Declares the validation rules.
		*/
		public function rules()
		{
			return array(
				array('name, email, message', 'required'),
				array('name, email, subject', 'length', 'max'=>255),
				array('email', 'email','message'=>"Не корректный email"),
			);
		}

		/**
		* Declares customized attribute labels.
		* If not declared here, an attribute would have a label that is
		* the same as its name with the first letter in upper case.
		*/
		public function attributeLabels()
		{
			return array(
				'name'=>'Ваше Имя',
				'email' => 'Ваш Email',
				'subject' => 'Тема сообщения',
				'message' => 'Ваше сообщение',
			);
		}
		
		public function save() {
			Yii::app()->config->set('name', $this->name);
			Yii::app()->config->set('email', $this->email);
			Yii::app()->config->set('subject', $this->subject);
			Yii::app()->config->set('message', $this->message);
			return true;
		}
		
	}
