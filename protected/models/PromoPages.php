<?php

/**
 * This is the model class for table "promo_pages".
 *
 * The followings are the available columns in table 'promo_pages':
 * @property string $id
 * @property integer $page
 * @property string $name_uk
 * @property string $name_ru
 * @property string $description_uk
 * @property string $description_ru
 * @property integer $timeupdate
 * @property integer $sort
 */
class PromoPages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'promo_pages';
	}
	
	public $image=false;
	 
 	public function behaviors(){
    return array(
      'imageBehavior' => array(
        'class' => 'ImageBehavior',
        'imagePath' => '/images/promo_pages/',
        'imageField' => 'image',
        'imageFolderName' => 'promo_pages',
      ),
    );
  }
  
  public function beforeSave(){
		if(parent::beforeSave())
    {
			if($this->isNewRecord){
				$this->setAttribute('sort', $this->count()+1);
			} else{
			}
			$this->setAttribute('timeupdate', TIME);
			
			return true;
    }
    else
        return false;
	}
	
	public function afterSave(){
		parent::afterSave();
	  
	  //Upload picture
		$this->image=CUploadedFile::getInstance($this,'image');
		if($this->image){
			$imagePath = $this->getPhotoPath($this->id);
			$this->image->saveAs($imagePath);
		}
	}  

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page, name_uk, name_ru', 'required'),
			array('page, timeupdate, sort', 'numerical', 'integerOnly'=>true),
			array('name_uk, name_ru, description_uk, description_ru', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, page, name_uk, name_ru, description_uk, description_ru, timeupdate, sort', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page' => 'Page',
			'name_uk' => 'Name Uk',
			'name_ru' => 'Name Ru',
			'description_uk' => 'Description Uk',
			'description_ru' => 'Description Ru',
			'timeupdate' => 'Timeupdate',
			'sort' => 'Sort',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('page',$this->page);
		$criteria->compare('name_uk',$this->name_uk,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('description_uk',$this->description_uk,true);
		$criteria->compare('description_ru',$this->description_ru,true);
		$criteria->compare('timeupdate',$this->timeupdate);
		$criteria->compare('sort',$this->sort);
		if(!isset($_GET['PromoPages_sort'])){
			$criteria->order = 'sort asc';
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PromoPages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
