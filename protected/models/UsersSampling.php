<?php

/**
 * This is the model class for table "users_sampling".
 *
 * The followings are the available columns in table 'users_sampling':
 * @property string $id
 * @property string $fio
 * @property string $email
 * @property string $phone
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $plate
 * @property string $postindex
 * @property string $status_delivery
 */
class UsersSampling extends CActiveRecord
{
	public $agree;
	public $phoneCode = array('050','066','095','099','063','093','096');
	public $verifyCode;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users_sampling';
	}
	
	public function beforeSave(){
		if(parent::beforeSave())
    {
			if($this->isNewRecord){
				if(empty($this->status_delivery)){
					$this->status_delivery = 1; // Status = 1-New
				}
			} else{
			}
			
			return true;
    }
    else
        return false;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fio, email, phone, region, city, street, house, plate, postindex, status_delivery', 'required', 'message'=>Yii::t('Text','Поле не должно быть пустым')),
			array('fio, email, region, city, street', 'length', 'max'=>255, 'message'=>Yii::t('Text','Разрешено не более 255 символов')),
			array('house, plate, postindex', 'length', 'max'=>20, 'message'=>Yii::t('Text','Разрешено не более 20 символов')),
			array('phone, plate, postindex', 'numerical', 'integerOnly'=>true, 'message'=>Yii::t('Text','Только числа')),			
			array('phone', 'length', 'min'=>10,'max'=>12,'tooLong'=>Yii::t('Text','Не более 12 символов'),'tooShort'=>Yii::t('Text','Не менее 10 символов')),
			array('postindex', 'length', 'min'=>4,'max'=>6,'tooLong'=>Yii::t('Text','Не более 6 символов'),'tooShort'=>Yii::t('Text','Не менее 4 символов')),
			array('status_delivery', 'length', 'max'=>1),
			array('status_delivery', 'in', 'range'=>range(1,3)),
			array('email', 'email', 'message'=>Yii::t('Text','Email не валидный')),
			array('email', 'unique','message'=>Yii::t('Text','E-mail уже участвует в акции')),
			array('phone', 'unique','message'=>Yii::t('Text','Номер уже участвует в акции')),	
			
			array('agree', 'compare', 'compareValue' => true, 'message' => 'You must agree to the terms and conditions', 'on' =>'addSampling'),					
			array('verifyCode', 'CaptchaExtendedValidator', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on' =>'addSampling'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fio, email, phone, region, city, street, house, plate, postindex, status_delivery', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fio' => Yii::t('Text','ФИО'),
			'email' => Yii::t('Text','Email'),
			'phone' => Yii::t('Text','Телефон'),
			'region' => Yii::t('Text','Область'),
			'city' => Yii::t('Text','Город'),
			'street' => Yii::t('Text','Улица'),
			'house' => Yii::t('Text','Дом'),
			'plate' => Yii::t('Text','Квартира'),
			'postindex' => Yii::t('Text','Индекс'),
			'status_delivery' => Yii::t('Text','Status Delivery'),
			'verifyCode' => Yii::t('Text','Код проверки'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('region',$this->region,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('house',$this->house,true);
		$criteria->compare('plate',$this->plate,true);
		$criteria->compare('postindex',$this->postindex,true);
		$criteria->compare('status_delivery',$this->status_delivery,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersSampling the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
