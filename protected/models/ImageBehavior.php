<?php
class ImageBehavior extends CActiveRecordBehavior
{
    public $imagePath = '';
    public $imageField = '';
    public $imageFolderName = '';
 
    public function getImageUrl(){
    	return $this->getBaseImagePath() . $this->owner->{$this->imageField};
    }
 
    public function getImageThumbUrl(){
      return $this->getBaseImagePath() . 'small_' . $this->owner->{$this->imageField};
    }
 
    public function getBaseImagePath(){
      return Yii::app()->request->baseUrl . '/' . $this->imagePath . '/';
    }  
    
    public function getPhotoUrl($id=false, $updateTime=false){
			if(is_file($this->getPhotoPath($id))){
				return Yii::app()->request->baseUrl.$this->imagePath.$id.'.jpg'.($updateTime ? '?'.$updateTime : '');
			}
			return false;
		}
		
		public function getPhotoPath($id=false){
			return Yii::getPathOfAlias('webroot.images.'.$this->imageFolderName.'.'.$id).'.jpg';	
		}
		
		public function getPhotoNameUrl($name=false, $updateTime=false){
			if(is_file($this->getPhotoNamePath($name))){
				return Yii::app()->request->baseUrl.$this->imagePath.$name.($updateTime ? '?'.$updateTime : '');
			}
			return false;
		}
		
		public function getPhotoNamePath($name=false){
			return Yii::getPathOfAlias('webroot.images.'.$this->imageFolderName).'/'.$name;	
		}
}