<?php

/**
 * This is the model class for table "video".
 *
 * The followings are the available columns in table 'video':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $youtubeurl
 * @property integer $duraction
 * @property string $timecreate
 * @property string $timeupdate
 * @property string $sort
 * @property integer $status
 */
class Video extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'video';
	}
	
	public function beforeSave(){
		if(parent::beforeSave())
    {
    	$this->youtubeurl = CDates::youtubelinkParser($this->youtubeurl);
			if($this->isNewRecord){
				$this->setAttribute('timecreate', TIME);
				$this->setAttribute('timeupdate', TIME);
				$this->setAttribute('sort', 0);
			} else{
				$this->setAttribute('timeupdate', TIME);
			}
			
			return true;
    }
    else
    	return false;
	}
	
	public function afterSave(){
		parent::afterSave();
    
    if ($this->isNewRecord) {
			//Update sort in All Rows
			$connection=Yii::app()->db;
			$sql="UPDATE {$this->tableName()} set sort=sort+1";
			$command=$connection->createCommand($sql)->execute();
    }

	}	

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, youtubeurl, status, id_category', 'required'),
			array('duraction, status', 'numerical', 'integerOnly'=>true),
			array('name, description, youtubeurl', 'length', 'max'=>255),
			array('timecreate, timeupdate, sort, id_category', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, youtubeurl, duraction, timecreate, timeupdate, sort, status, id_category', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'video0' => array(self::HAS_ONE, 'VideoCategory', array('id'=>'id_category')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'description' => 'Описание',
			'youtubeurl' => 'Youtube url',
			'duraction' => 'Продолжительность',
			'timecreate' => 'Timecreate',
			'timeupdate' => 'Timeupdate',
			'sort' => 'Сортировка',
			'status' => 'Статус',
			'id_category' => 'Категория',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('youtubeurl',$this->youtubeurl,true);
		$criteria->compare('duraction',$this->duraction);
		$criteria->compare('timecreate',$this->timecreate,true);
		$criteria->compare('timeupdate',$this->timeupdate,true);
		$criteria->compare('sort',$this->sort,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('id_category',$this->id_category);
		if(!isset($_GET['Video_sort'])){
			$criteria->order = 'sort ASC';	
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Video the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
