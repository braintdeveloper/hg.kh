<?php

/**
 * This is the model class for table "products_to_used".
 *
 * The followings are the available columns in table 'products_to_used':
 * @property string $id
 * @property string $idproducts
 * @property string $idused
 *
 * The followings are the available model relations:
 * @property ProductsUsed $idused0
 * @property Products $idproducts0
 */
class ProductsToUsed extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_to_used';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idproducts, idused', 'required'),
			array('idproducts, idused', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idproducts, idused', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idused0' => array(self::BELONGS_TO, 'ProductsUsed', 'idused'),
			'idproducts0' => array(self::BELONGS_TO, 'Products', 'idproducts'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idproducts' => 'Idproducts',
			'idused' => 'Idused',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idproducts',$this->idproducts,true);
		$criteria->compare('idused',$this->idused,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductsToUsed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
