<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $newpassword
 * @property integer $confirmpassword
 * @property integer $level
 */
class User extends CActiveRecord
{
	public $password2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}
	
	public function matchPassword(){
		//Password
		if(!$this->isNewRecord) {
			if(empty($this->password)){
				unset($this->password);
			}
		} else {
			if(empty($this->password)){
				$this->addError('password', 'Please enter a password');
			}
		}

		if(!empty($this->password)) {
			if($this->password != $this->password2){
				//$this->createValidators();
				$this->addError('password2', 'Passwords must match');
			}
			$this->password = crypt($this->password,SALT);
		}
		
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username', 'required'),
			array('password', 'matchPassword'),
			array('confirmpassword, level', 'numerical', 'integerOnly'=>true),
			array('username, password, newpassword', 'length', 'max'=>256),
			array('password, password2', 'length', 'min'=>6),
			array('username', 'email','message'=>"Не корректный email"),
      array('username', 'unique','message'=>'Такой юзер уже существует'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, newpassword, confirmpassword, level', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'User Name',
			'password' => 'Password',
			'password2' => 'Reply Password',
			'newpassword' => 'Newpassword',
			'confirmpassword' => 'Confirmpassword',
			'level' => 'Level',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('newpassword',$this->newpassword,true);
		$criteria->compare('confirmpassword',$this->confirmpassword);
		$criteria->compare('level',$this->level);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function isAccess($id=false, $level=false){
		if((int)$id && (int)$level){
			$user = $this->find('id=:id',array(':id'=>$id));	
			if($user->level>=$level){
				return true;
			}
		}
		return false;		
	}
}
