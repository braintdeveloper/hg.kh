<?php

/**
 * This is the model class for table "questions".
 *
 * The followings are the available columns in table 'questions':
 * @property string $id
 * @property string $idcategory
 * @property string $alias
 * @property string $htitl_uk
 * @property string $htitl_ru
 * @property string $hdesc_uk
 * @property string $hdesc_ru
 * @property string $hkeyw_uk
 * @property string $hkeyw_ru
 * @property string $name_uk
 * @property string $name_ru
 * @property string $content_uk
 * @property string $content_ru
 * @property integer $status
 */
class Questions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'questions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_uk, alias', 'required'),
			array('status, idcategory', 'numerical', 'integerOnly'=>true),
			array('idcategory', 'length', 'max'=>11),
			array('alias, htitl_uk, htitl_ru, hdesc_uk, hdesc_ru, hkeyw_uk, hkeyw_ru, name_uk, name_ru', 'length', 'max'=>256),
			array('content_uk, content_ru', 'safe'),
			
			array('alias', 'unique', 'className' => 'Questions', 'message' => 'This alias is already in database'),
			array('alias', 'match', 'pattern' => '/^[a-z0-9_\-]+$/u', 'message' => "Incorrect symbols. Allow only small english letters and numbers"),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idcategory, alias, htitl_uk, htitl_ru, hdesc_uk, hdesc_ru, hkeyw_uk, hkeyw_ru, name_uk, name_ru, content_uk, content_ru, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'questionsCategory0' => array(self::HAS_ONE, 'QuestionsCategory', array('id'=>'idcategory')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idcategory' => 'Idcategory',
			'alias' => 'Alias',
			'htitl_uk' => 'Htitl Uk',
			'htitl_ru' => 'Htitl Ru',
			'hdesc_uk' => 'Hdesc Uk',
			'hdesc_ru' => 'Hdesc Ru',
			'hkeyw_uk' => 'Hkeyw Uk',
			'hkeyw_ru' => 'Hkeyw Ru',
			'name_uk' => 'Name Uk',
			'name_ru' => 'Name Ru',
			'content_uk' => 'Content Uk',
			'content_ru' => 'Content Ru',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idcategory',$this->idcategory,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('htitl_uk',$this->htitl_uk,true);
		$criteria->compare('htitl_ru',$this->htitl_ru,true);
		$criteria->compare('hdesc_uk',$this->hdesc_uk,true);
		$criteria->compare('hdesc_ru',$this->hdesc_ru,true);
		$criteria->compare('hkeyw_uk',$this->hkeyw_uk,true);
		$criteria->compare('hkeyw_ru',$this->hkeyw_ru,true);
		$criteria->compare('name_uk',$this->name_uk,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('content_uk',$this->content_uk,true);
		$criteria->compare('content_ru',$this->content_ru,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Questions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
