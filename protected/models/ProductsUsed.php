<?php

/**
 * This is the model class for table "products_used".
 *
 * The followings are the available columns in table 'products_used':
 * @property string $id
 * @property string $name_uk
 * @property string $name_ru
 *
 * The followings are the available model relations:
 * @property ProductsToUsed[] $productsToUseds
 */
class ProductsUsed extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_used';
	}
	
	public $image=false;
 
 	public function behaviors(){
    return array(
      'imageBehavior' => array(
        'class' => 'ImageBehavior',
        'imagePath' => '/images/products_used/',
        'imageField' => 'image',
        'imageFolderName' => 'products_used',
      ),
    );
  }
  
	public function beforeSave(){
		if(parent::beforeSave())
    {
			if($this->isNewRecord){
				$this->setAttribute('timecreate', TIME);
				$this->setAttribute('timeupdate', TIME);
			} else{
				$this->setAttribute('timeupdate', TIME);
			}
			
			return true;
    }
    else
        return false;
	}  
  
  public function afterSave(){
		parent::afterSave();
    
    //Upload picture
		$this->image=CUploadedFile::getInstance($this,'image');
		if($this->image){
			$imagePath = $this->getPhotoPath($this->id);
			$this->image->saveAs($imagePath);
		}
  }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_uk', 'required'),
			array('name_uk, name_ru', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_uk, name_ru', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productsToUseds' => array(self::HAS_MANY, 'ProductsToUsed', 'idused'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_uk' => 'Name Uk',
			'name_ru' => 'Name Ru',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name_uk',$this->name_uk,true);
		$criteria->compare('name_ru',$this->name_ru,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductsUsed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
