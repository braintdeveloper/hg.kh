<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property string $id
 * @property integer $idquestion
 * @property integer $idbanner1
 * @property integer $idbanner2
 * @property string $alias
 * @property string $price
 * @property string $htitl_uk
 * @property string $htitl_ru
 * @property string $hdesc_uk
 * @property string $hdesc_ru
 * @property string $hkeyw_uk
 * @property string $hkeyw_ru
 * @property string $name_uk
 * @property string $name_ru
 * @property string $anons_uk
 * @property string $anons_ru
 * @property string $anons2_uk
 * @property string $anons2_ru
 * @property string $content_uk
 * @property string $content_ru
 * @property string $help1_uk
 * @property string $help1_ru
 * @property string $help2_uk
 * @property string $help2_ru
 * @property string $help3_uk
 * @property string $help3_ru
 * @property string $timecreate
 * @property string $timeupdate
 * @property integer $status
 * @property string $sort
 */
class Products extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}
	
	public $productsused=false;
	public $pdf=false;
	public $image=false;
	public $image2=false;
	public $image3=false;
 
 	public function behaviors(){
    return array(
      'imageBehavior' => array(
        'class' => 'ImageBehavior',
        'imagePath' => '/images/products/',
        'imageField' => 'image',
        'imageFolderName' => 'products',
      ),
    );
  }
	
	public function beforeSave(){
		if(parent::beforeSave())
    {
			if($this->isNewRecord){
				$this->setAttribute('timecreate', TIME);
				$this->setAttribute('timeupdate', TIME);
				$this->setAttribute('sort', 0);
			} else{
				$this->setAttribute('timeupdate', TIME);
				ProductsToUsed::model()->deleteAll('idproducts='.$this->id);
			}
			
			foreach((array)@$_POST['Products']['productsused'] as $gtc){
				$model = new ProductsToUsed();
				$model->idproducts = $this->id;
				$model->idused = $gtc;
				$model->save();
			}
			
			return true;
    }
    else
        return false;
	}
	
	public function afterSave(){
		parent::afterSave();
    
    if ($this->isNewRecord) {
			//Update sort in All Rows
			$connection=Yii::app()->db;
			$sql="UPDATE {$this->tableName()} set sort=sort+1";
			$command=$connection->createCommand($sql)->execute();
    }
    
    //Upload pdf
		$this->pdf=CUploadedFile::getInstance($this,'pdf');
		if($this->pdf){
			$imagePath = $this->getPhotoNamePath($this->id.'.pdf');
			$this->pdf->saveAs($imagePath);
		}
    
    //Upload picture
		$this->image=CUploadedFile::getInstance($this,'image');
		if($this->image){
			$imagePath = $this->getPhotoPath($this->id);
			$this->image->saveAs($imagePath);
		}
		
		//Upload picture 2
		$this->image2=CUploadedFile::getInstance($this,'image2');
		if($this->image2){
			$imagePath = $this->getPhotoNamePath($this->id.'-2.jpg');
			$this->image2->saveAs($imagePath);
		}
		
		//Upload picture 2
		$this->image3=CUploadedFile::getInstance($this,'image3');
		if($this->image3){
			$imagePath = $this->getPhotoNamePath($this->id.'-3.jpg');
			$this->image3->saveAs($imagePath);
		}
	}	


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_uk, alias', 'required'),
			array('idquestion, idbanner1, idbanner2, status', 'numerical', 'integerOnly'=>true),
			array('alias, htitl_uk, htitl_ru, hdesc_uk, hdesc_ru, hkeyw_uk, hkeyw_ru, name_uk, name_ru, mainname_uk, mainname_ru, anonsname_uk, anonsname_ru, text1_uk, text1_ru', 'length', 'max'=>255),
			array('price', 'length', 'max'=>10),
			array('cents', 'length', 'max'=>2),
			array('timecreate, timeupdate, sort', 'length', 'max'=>10),
			array('sample, anons_uk, anons_ru, content_uk, content_ru, anonstext_uk, anonstext_ru,', 'safe'),
			array('alias', 'unique', 'className' => 'Products', 'message' => 'This alias is already in database'),
			array('alias', 'match', 'pattern' => '/^[a-z0-9_\-]+$/u', 'message' => "Incorrect symbols. Allow only small english letters and numbers"),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idquestion, idbanner1, idbanner2, idbanner3, alias, price, htitl_uk, htitl_ru, hdesc_uk, hdesc_ru, hkeyw_uk, hkeyw_ru, name_uk, name_ru, content_uk, content_ru, text1_uk, text1_ru, mainname_uk, mainname_ru, anonsname_uk, anonsname_ru, anonstext_uk, anonstext_ru, timecreate, timeupdate, status, sort, sample', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productsToUsed0' => array(self::MANY_MANY, 'ProductsUsed', 'products_to_used(idproducts, idused)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idquestion' => 'Idquestion',
			'idbanner1' => 'Idbanner1',
			'idbanner2' => 'Idbanner2',
			'alias' => 'Alias',
			'price' => 'Цена',
			'cents' => 'Копейки',
			'htitl_uk' => 'Htitl Uk',
			'htitl_ru' => 'Htitl Ru',
			'hdesc_uk' => 'Hdesc Uk',
			'hdesc_ru' => 'Hdesc Ru',
			'hkeyw_uk' => 'Hkeyw Uk',
			'hkeyw_ru' => 'Hkeyw Ru',
			'name_uk' => 'Name Uk',
			'name_ru' => 'Name Ru',
			'content_uk' => 'Content Uk',
			'content_ru' => 'Content Ru',
			'text1_uk' => 'Фраза1 Uk',
			'text1_ru' => 'Фраза1 Ru',
			'mainname_uk' => 'На главной - Имя Uk',
			'mainname_ru' => 'На главной - Имя Ru',
			'anonsname_uk' => 'Список - Имя Uk',
			'anonsname_ru' => 'Список - Имя Ru',
			'anonstext_uk' => 'Список - Текст Uk',
			'anonstext_ru' => 'Список - Текст Ru',
			'timecreate' => 'Timecreate',
			'timeupdate' => 'Timeupdate',
			'status' => 'Status',
			'sort' => 'Sort',
			'productsused' => 'Used with:',
			'sample' => 'Заказать семпл',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	
	public function search()
  {
    // @todo Please modify the following code to remove attributes that should not be searched.

    $criteria=new CDbCriteria;

    $criteria->compare('id',$this->id,true);
    $criteria->compare('idquestion',$this->idquestion);
    $criteria->compare('idbanner1',$this->idbanner1);
    $criteria->compare('idbanner2',$this->idbanner2);
    $criteria->compare('idbanner3',$this->idbanner3);
    $criteria->compare('alias',$this->alias,true);
    $criteria->compare('price',$this->price,true);
    $criteria->compare('htitl_uk',$this->htitl_uk,true);
    $criteria->compare('htitl_ru',$this->htitl_ru,true);
    $criteria->compare('hdesc_uk',$this->hdesc_uk,true);
    $criteria->compare('hdesc_ru',$this->hdesc_ru,true);
    $criteria->compare('hkeyw_uk',$this->hkeyw_uk,true);
    $criteria->compare('hkeyw_ru',$this->hkeyw_ru,true);
    $criteria->compare('name_uk',$this->name_uk,true);
    $criteria->compare('name_ru',$this->name_ru,true);
    $criteria->compare('content_uk',$this->content_uk,true);
    $criteria->compare('content_ru',$this->content_ru,true);
    $criteria->compare('text1_uk',$this->text1_uk,true);
    $criteria->compare('text1_ru',$this->text1_ru,true);
    $criteria->compare('mainname_uk',$this->mainname_uk,true);
    $criteria->compare('mainname_ru',$this->mainname_ru,true);
    $criteria->compare('anonsname_uk',$this->anonsname_uk,true);
    $criteria->compare('anonsname_ru',$this->anonsname_ru,true);
    $criteria->compare('anonstext_uk',$this->anonstext_uk,true);
    $criteria->compare('anonstext_ru',$this->anonstext_ru,true);
    $criteria->compare('timecreate',$this->timecreate,true);
    $criteria->compare('timeupdate',$this->timeupdate,true);
    $criteria->compare('status',$this->status);
    $criteria->compare('sort',$this->sort,true);
    $criteria->compare('sample',$this->sample);
		if(!isset($_GET['Products_sort'])){
			$criteria->order = 'sort ASC';	
		}
    
    return new CActiveDataProvider($this, array(
        'criteria'=>$criteria,
    ));
  }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
