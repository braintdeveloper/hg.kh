<?php

/**
 * This is the model class for table "articles".
 *
 * The followings are the available columns in table 'articles':
 * @property string $id
 * @property string $idcategory
 * @property string $alias
 * @property string $htitl_uk
 * @property string $htitl_ru
 * @property string $hdesc_uk
 * @property string $hdesc_ru
 * @property string $hkeyw_uk
 * @property string $hkeyw_ru
 * @property string $name_uk
 * @property string $name_ru
 * @property string $anons_uk
 * @property string $anons_ru
 * @property string $content_uk
 * @property string $content_ru
 * @property string $bannerlink
 * @property integer $timecreate
 * @property integer $timeupdate
 * @property integer $status
 */
class Articles extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'articles';
	}
	
	public $image_sliderBg;
	public $image_slider;
	public $image;
 
 	public function behaviors(){
    return array(
      'imageBehavior' => array(
        'class' => 'ImageBehavior',
        'imagePath' => '/images/articles/',
        'imageField' => 'image',
        'imageFolderName' => 'articles',
      ),
    );
  }
	
	public function beforeSave(){
		if(parent::beforeSave())
    {
    	$this->setAttribute('timework', CDates::toUnixDate($this->timework));
			if($this->isNewRecord){
				$this->setAttribute('timecreate', TIME);
				$this->setAttribute('timeupdate', TIME);
				$this->setAttribute('sort', 0);
			} else{
				$this->setAttribute('timeupdate', TIME);
			}
			
			return true;
    }
    else
        return false;
	}
	
	public function afterSave(){
		parent::afterSave();
    
    if ($this->isNewRecord) {
			//Update sort in All Rows
			$connection=Yii::app()->db;
			$sql="UPDATE {$this->tableName()} set sort=sort+1";
			$command=$connection->createCommand($sql)->execute();
    }
    
    //Upload picture
    $this->image=CUploadedFile::getInstance($this,'image');
		if($this->image){
			$imagePath = $this->getPhotoNamePath($this->id.'.jpg');
			$this->image->saveAs($imagePath);
		}
		//Upload image_sliderBg
		$this->image_sliderBg=CUploadedFile::getInstance($this,'image_sliderBg');
		if($this->image_sliderBg){
			$imagePath = $this->getPhotoNamePath($this->id.'.sBg.jpg');
			$this->image_sliderBg->saveAs($imagePath);
		}
		//Upload image_slider
    $this->image_slider=CUploadedFile::getInstance($this,'image_slider');
		if($this->image_slider){
			$imagePath = $this->getPhotoNamePath($this->id.'.s.jpg');
			$this->image_slider->saveAs($imagePath);
		}

	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_ru, name2_ru, alias', 'required'),
			array('status, onslider, views, id_author', 'numerical', 'integerOnly'=>true),
			array('alias, htitl_ru, hdesc_ru, hkeyw_ru, name_ru', 'length', 'max'=>256),
			array('timecreate, timeupdate, timework', 'length', 'max'=>10),
			array('views', 'length', 'max'=>11),
			array('anons_ru, content_ru', 'safe'),
			array('alias', 'unique', 'className' => 'Articles', 'message' => 'This alias is already in database'),
			array('alias', 'match', 'pattern' => '/^[a-z0-9_\-]+$/u', 'message' => "Incorrect symbols. Allow only small english letters and numbers"),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idcategory, alias, htitl_uk, htitl_ru, hdesc_uk, hdesc_ru, hkeyw_uk, hkeyw_ru, name_uk, name_ru, name2_ru, anons_uk, anons_ru, content_uk, content_ru, timecreate, timeupdate, timework, status, onslider,views', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'articlesCategory0' => array(self::HAS_ONE, 'ArticlesCategory', array('id'=>'idcategory')),
			'articlesAuthor' => array(self::HAS_ONE, 'Authors', array('id'=>'id_author')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'alias' => 'Урл',
			'id_category' => 'Категория',
			'id_author' => 'Автор',
			'htitl_ru' => 'Метатег Title',
			'hdesc_ru' => 'Метатег Description',
			'hkeyw_ru' => 'Метатег Keywords',
			'name_ru' => 'Имя',
			'name2_ru' => 'Описание',
			'anons_ru' => 'Анонс',
			'content_ru' => 'Контент',
			'timecreate' => 'Timecreate',
			'timeupdate' => 'Timeupdate',
			'timework' => 'Дата начала',
			'status' => 'Статус',
			'onslider' => 'На_главную (статус)',
			'views' => 'Просмотры',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('htitl_ru',$this->htitl_ru,true);
		$criteria->compare('hdesc_ru',$this->hdesc_ru,true);
		$criteria->compare('hkeyw_ru',$this->hkeyw_ru,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('anons_ru',$this->anons_ru,true);
		$criteria->compare('content_ru',$this->content_ru,true);
		$criteria->compare('timecreate',$this->timecreate,true);
		$criteria->compare('timeupdate',$this->timeupdate,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('onslider',$this->onslider);
		$criteria->compare('views',$this->views);
		$criteria->compare('id_author',$this->id_author);
		if(!isset($_GET['Articles_sort'])){
			$criteria->order = 'timework DESC';	
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Articles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
