<?php

/**
 * This is the model class for table "promo_texts".
 *
 * The followings are the available columns in table 'promo_texts':
 * @property string $id
 * @property string $name_uk
 * @property string $name_ru
 * @property string $text_uk
 * @property string $text_ru
 * @property string $text2_uk
 * @property string $text2_ru
 * @property integer $sort
 * @property integer $timeupdate
 */
class PromoTexts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'promo_texts';
	}
	
	public $image=false;
	 
 	public function behaviors(){
    return array(
      'imageBehavior' => array(
        'class' => 'ImageBehavior',
        'imagePath' => '/images/promo_texts/',
        'imageField' => 'image',
        'imageFolderName' => 'promo_texts',
      ),
    );
  }
  
  public function beforeSave(){
		if(parent::beforeSave())
    {
			if($this->isNewRecord){
				$this->setAttribute('sort', $this->count()+1);
			} else{
			}
			$this->setAttribute('timeupdate', TIME);
			
			return true;
    }
    else
        return false;
	}
	
	public function afterSave(){
		parent::afterSave();
	  
	  //Upload picture
		$this->image=CUploadedFile::getInstance($this,'image');
		if($this->image){
			$imagePath = $this->getPhotoPath($this->id);
			$this->image->saveAs($imagePath);
		}
	}	

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_ru, name_uk, name_ru', 'required'),
			array('sort, timeupdate', 'numerical', 'integerOnly'=>true),
			array('name_uk, name_ru', 'length', 'max'=>255),
			array('text_uk, text_ru, text2_uk, text2_ru', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_uk, name_ru, text_uk, text_ru, text2_uk, text2_ru, sort, timeupdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_uk' => 'Name Uk',
			'name_ru' => 'Name Ru',
			'text_uk' => 'Text Uk',
			'text_ru' => 'Text Ru',
			'text2_uk' => 'Text2 Uk',
			'text2_ru' => 'Text2 Ru',
			'sort' => 'Sort',
			'timeupdate' => 'Timeupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name_uk',$this->name_uk,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('text_uk',$this->text_uk,true);
		$criteria->compare('text_ru',$this->text_ru,true);
		$criteria->compare('text2_uk',$this->text2_uk,true);
		$criteria->compare('text2_ru',$this->text2_ru,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('timeupdate',$this->timeupdate);
		if(!isset($_GET['PromoTexts_sort'])){
			$criteria->order = 'sort asc';
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PromoTexts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
