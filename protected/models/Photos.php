<?php

/**
 * This is the model class for table "photos".
 *
 * The followings are the available columns in table 'photos':
 * @property string $id
 * @property string $id_parent
 * @property string $name
 * @property string $timecreate
 * @property string $timeupdate
 * @property integer $status
 * @property string $sort
 *
 * The followings are the available model relations:
 * @property Portfolio $idPortfolio
 */
class Photos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'photos';
	}
	
	public $image=false;
 
 	public function behaviors(){
    return array(
        'imageBehavior' => array(
            'class' => 'ImageBehavior',
            'imagePath' => '/images/photos/',
            'imageField' => 'image',
            'imageFolderName' => 'photos',
        ),
    );
  }	
  
  
	public function beforeSave(){
		if(parent::beforeSave())
    {
			if($this->isNewRecord){
				$this->setAttribute('timecreate', TIME);
				$this->setAttribute('timeupdate', TIME);
				$this->setAttribute('sort', 0);
				$this->setAttribute('status', 1);
			} else{
				$this->setAttribute('timeupdate', TIME);
			}
			
			return true;
    }
    else
        return false;
	}
	
	public function afterSave(){
		parent::afterSave();
    if ($this->isNewRecord) {    	
			//Update sort in All Rows
			$connection=Yii::app()->db;
			$sql="UPDATE {$this->tableName()} set sort=sort+1 WHERE id_parent = {$this->id_parent}";
			$command=$connection->createCommand($sql)->execute();
    }
	}
	  

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_parent, type', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('type', 'in', 'range'=>array('article','team')),
			array('id_parent', 'length', 'max'=>11),
			array('name', 'length', 'max'=>256),
			array('timecreate, timeupdate, sort', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_parent, type, name, timecreate, timeupdate, status, sort', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'articles' => array(self::BELONGS_TO, 'Articles', 'id_parent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_parent' => 'Id Родитель',
			'name' => 'Имя',
			'timecreate' => 'Timecreate',
			'timeupdate' => 'Timeupdate',
			'status' => 'Статус',
			'sort' => 'Сортировка',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id_parent=false, $type=false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if((int)$id_parent && $type){
			$this->id_parent = $id_parent;	
			$this->type = $type;
		}
		$criteria->compare('id',$this->id,true);
		$criteria->compare('id_parent',$this->id_parent,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('timecreate',$this->timecreate,true);
		$criteria->compare('timeupdate',$this->timeupdate,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort,true);
		if(!isset($_GET['Photos_sort'])){
			$criteria->order = 'sort ASC';	
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Photos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function deletePhotos($parent, $type){
		if((int)$parent && $type){
			$model = Photos::model()->findAll('id_parent=:parent and type=:type', array(':parent'=>$parent,':type'=>$type));
			foreach($model as $r){
				@unlink(Photos::model()->getPhotoPath($r->id));
			}
			Photos::model()->deleteAll('id_parent=:parent and type=:type', array(':parent'=>$parent,':type'=>$type));
		}
	}
	
}
