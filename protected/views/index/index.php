<!-- SLIDER -->
<div class="slider-1 clearfix">

	<div class="flex-container">
		<div class="flexslider loading">
			<ul class="slides">

			<?php $anonsList = Articles::model()->findAll('status=1 AND timework > '.TIME.' ORDER BY timework ASC');?>
			<?php //if(!$anonsList) {
				$firstList['0'] = Articles::model()->findByPk(1) ;
				$anonsList = array_merge($anonsList, $firstList) ;
			//} ?>
			<?php foreach($anonsList as $row){ ?>
				<?php if($imgSliderBg = Articles::model()->getPhotoNameUrl($row->id.'.sBg.jpg', $row->timeupdate)){ ?>
				<li style="background:url(<?=$imgSliderBg?>) no-repeat;background-position:50% 0">
				<?php } else { ?>
				<li style="background:url('images/defaultSliderBg.jpg') no-repeat;background-position:50% 0">
				<?php } ?>
					<div class="container">
						<div class="sixteen columns contain">
							<h2 data-toptitle="50%"><?=$row->name_ru;?></h2>
							<p data-bottomtext="48%"><?=$row->name2_ru;?></p>
							<div class="links" data-bottomlinks="30%">
								<a class="button medium r-m-plus r-m-full" href="<?=$this->createUrl('/articles/view',array('alias'=>$row->alias));?>">ЧИТАТЬ ДАЛЕЕ</a>
							</div>
							<?php if($imgSlider = Articles::model()->getPhotoNameUrl($row->id.'.s.jpg', $row->timeupdate)){ ?>
							<img src="<?=$imgSlider;?>" class="item" alt="slide-item"  data-topimage="21%"/>
							<?php } ?>
						</div>
					</div><!-- End Container -->
				</li><!-- End item -->
			<?php } ?>

			</ul>
		</div>
	</div>

</div><!-- End slider -->


<!-- 3 BLOCKS (1 ver) -->
<div class="container clearfix min-m-top-75">
	<div class="sixteen columns m-bot-15">

		<?php /*<!-- BLOCK -->
		<div class="block-4-col m-bot-20 ca-menu">
			<div class="block-0-content-container">
				<div class="block-text">
					ТАКЖЕ РЕКОМЕНДУЕМ<br>
					ВАМ ПОСЕТИТЬ<br>
					<strong>СТРАНИЦЫ</strong>
				</div>
				<div class="block-r-m-container">
					<a class="button medium r-m-plus r-m-full" href="<?=$this->createUrl('/video');?>">ЧИТАТЬ ДАЛЕЕ</a>
				</div>
			</div>
		</div>*/?>

		<!-- BLOCK -->
		<div class="block-4-col m-bot-20 ca-menu">
			<div class="block-1-content-container">
				<a class=" clearfix" href="<?=$this->createUrl('/video');?>">
					<div class="ca-icon">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/icon-video.png" alt="Видеозаписи">
					</div>
					<div class="ca-content">
						<h2 class="ca-main">ВИДЕО</h2>
						<h3 class="ca-sub">Архив видеозаписей проповедей, токшоу и другое</h3>
					</div>
				</a>
			</div>
		</div>

		<!-- BLOCK  AUDIO -->
		<div class="block-4-col m-bot-20 ca-menu">
			<div class="block-2-content-container">
				<a class=" clearfix" href="<?=$this->createUrl('/audio');?>">
					<div class="ca-icon">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/icon-audio.png" alt="Аудиозаписи">
					</div>
					<div class="ca-content">
						<h2 class="ca-main">АУДИО</h2>
						<h3 class="ca-sub">Молодёжная группа прославления</h3>
					</div>
				</a>
			</div>
		</div>

		<!-- BLOCK  NEWS -->
		<div class="block-4-col m-bot-20 ca-menu">
			<div class="block-2-content-container">
				<a class=" clearfix" href="<?=$this->createUrl('/articles');?>">
					<div class="ca-icon">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/vid.png" alt="Новости">
					</div>
					<div class="ca-content">
						<h2 class="ca-main">НОВОСТИ</h2>
						<h3 class="ca-sub">Все события молодежного служения</h3>
					</div>
				</a>
			</div>
		</div>

		<!-- BLOCK -->
		<div class="block-4-col m-bot-20 ca-menu">
			<div class="block-3-content-container">
				<a class=" clearfix" href="<?=$this->createUrl('/church');?>">
					<div class="ca-icon">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/church.png" alt="Виды служений">
					</div>
					<div class="ca-content">
						<h2 class="ca-main">ВИДЫ СЛУЖЕНИЙ</h2>
						<h3 class="ca-sub">Приглашаем тебя учавствовать в служении</h3>
					</div>
				</a>
			</div>
		</div>

		<div class="clear"></div>
<!--		--><?php //if (date('w', TIME)== 0) { // Only Sunday ?>
		<iframe style="width: 100%" height="436" src="<?php echo Yii::app()->params['adminSettings']['9']; ?>" frameborder="0" allowfullscreen></iframe>
<!--		--><?php //} ?>
		<div class="styled-box iconed-box info"><strong>Здесь! Онлайн трансляция - Каждое воскресенье в 18:00</strong></div>


	</div>
</div>

</div>	<!-- Grey bg end -->




<?php /*<section class="main">

	<article class="slider">
	<?php foreach((array)Slider::model()->findAll('status=1') as $row) {?>
	<figure class="slide1 slide-id<?=$row->id;?>">
	<a href="<?=$row->link;?>">
	<img src="<?=Slider::model()->getPhotoNameUrl($row->id.'.'.Yii::app()->getLanguage().'.jpg', $row->timeupdate);?>" alt="<?=CHtml::encode($row->{'name'.DMultilangHelper::getPrefix()});?>">
	</a>
	</figure>
	<?php } ?>
	</article>

	<?php
	$criteria = new CDbCriteria;
	$criteria->condition = 't.status=:active';
	$criteria->params = array('active'=>1);
	$criteria->order = 'sort asc';
	$products = Products::model()->findAll($criteria);
	?>
	<?php foreach($products as $model){?>
	<div class="clavusblock">
	<img src="<?=$model->getPhotoUrl($model->id, $model->timeupdate)?>" alt="<?=CHtml::encode($model->{'mainname'.DMultilangHelper::getPrefix()});?>" onclick="window.location.href='<?=$this->createUrl('/products/view', array('alias'=>$model->alias));?>'" />
	<p><?=$model->{'mainname'.DMultilangHelper::getPrefix()};?></p>
	<a href="<?=$this->createUrl('/products/view', array('alias'=>$model->alias));?>"><?php echo Yii::t('Text', 'Подробнее'); ?></a>
	</div>
	<?php }?>

</section>*/?>