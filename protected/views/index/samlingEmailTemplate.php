<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>letter</title>
</head>
<body>
	<div style="width: 600px; background: #fff url(<?php echo Yii::app()->request->hostInfo . Yii::app()->request->baseUrl; ?>/emailTemplate/bg.jpg) no-repeat;">
		<table width="540" cellpadding="0" cellspacing="0" style="margin: auto; font-family: Tahoma, Arial;	font-size: 14px; color: #5d5d5d;">
			<tr>
				<td colspan="4" height="100">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4" align="center">
					<span style="color: #2ca3b6; text-transform: uppercase;	font-size: 24px; padding-bottom: 30px; display: block;">Доброго дня!</span>
				</td>
			</tr>
			<tr>
				<td width="20">&nbsp;</td>
				<td valign="top">
					<img src="<?php echo Yii::app()->request->hostInfo . Yii::app()->request->baseUrl; ?>/emailTemplate/item.jpg" style="margin-right: 20px;" alt="">
				</td>
				<td valign="top">
					<p style="margin: 0; line-height: 20px;">Ви щойно замовили безкоштовний зразок* пластиру Compeed®  на сайті www.compeеd.ua. Ваше замовлення буде доставлено протягом 22 календарних днів максимум до Вашої поштової скриньки за зазначеною Вами в заявці адресою.</p>
					<p style="margin: 1em 0 0; line-height: 20px;">Будь-які запитання стосовно доставки, будь-ласка, надсилайте за адресою: welcome@compeed.ua. </p>
					<p style="margin: 1em 0 0; line-height: 20px;">З повагою, <br>Команда Compeed®</p>
				</td>
				<td width="20">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4" height="85">&nbsp;</td>
			</tr>
			<tr>
				<td width="20"></td>
				<td colspan="2" height="85">
					<small style="font-size: 10px; line-height: 100%;">* Під безкоштовним зразком мається на увазі<br>   пластир COMPEED® від вологих молозів середній 1шт. </small>
				</td>
				<td width="20"></td>
			</tr>
			<tr>
				<td width="20"></td>
				<td colspan="2">
					<small style="font-size: 10px; line-height: 100%;">
						© ТОВ «Джонсон і Джонсон Україна» 2014 рік. Реклама виробу медичного призначення. <br>Цей сайт належить компанії ТОВ «Джонсон і Джонсон Україна», яка повністю відповідає за його вміст. Сайт орієнтований на фізичних і юридичних осіб з України. Промо-сайт виробів медичного призначення Compeed®. Перед застосуванням медичного виробу необхідно проконсультуватись з лікарем та слід обов’язково ознайомитись з інструкцією. С.Р. № 12433/2013 від 15.02.2013<br>Пластирі медичні Compeed®. Додаткова інформація надається за вимогою. ТОВ «Джонсон і Джонсон Україна» Україна, 02152, м. Київ, пр-т. Павла Тичини, 1В. Тел. +38 (044) 498 – 0 – 888.
					</small>
					<br><br>
				</td>
				<td width="20"></td>
			</tr>
			<tr>
				<td colspan="4" align="center">
					<img src="<?php echo Yii::app()->request->hostInfo . Yii::app()->request->baseUrl; ?>/emailTemplate/selfmedication.jpg" alt="">
				</td>
			</tr>
		</table>
	</div>
</body>
</html>