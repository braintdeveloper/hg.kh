	<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
				<h1 class="page-title"><span class="bold">Ошибка!</span> <?php echo CHtml::encode($message); ?>!</h1>
			</div>	
		</div>
	</div>	

</div>	<!-- Grey bg end -->	

<!-- 404 -->
<div class="container m-bot-100 m-top-80 clearfix">
	<div class="eight columns ">
		<div class="error404-numb">
			404
		</div>
	</div>
	<div class="eight columns ">
		<div class="error404-text">
			ОШИБКА<br> СТРАНИЦА НЕ<br> НАЙДЕНА
		</div>
	</div>
</div>


<!-- 404 2 -->
<div class="light-grey-bg m-top-30">
	<div class="container clearfix pad-t-b-30">
		<div class="sixteen columns content-container-white ">
			<div class="error404-main-text">
				<h2><span class="bold">Вы можете</span> посмотреть наши <span class="bold"><a href="<?=$this->createUrl('/video');?>">видео</a></span>, или почитать наши <span class="bold"><a href="<?=$this->createUrl('/articles');?>">новости</a></span></h2>
			</div>
		</div>
	</div>
</div>

<?php /*<?php
	$this->pageTitle=Yii::app()->name . ' - Error';
	$this->breadcrumbs=array(
	'Error',
	);
	?>

	<h2>Error <?php echo $code; ?></h2>

	<div class="error">
	<?php echo CHtml::encode($message); ?>
</div>*/?>