<div class="blog-item m-bot-35 clearfix">
	<div class="hover-item">
		<div class="clearfix">
			<div class="blog-item-date-inf-container left">
				<?php if($data->id != 1){ ?>
				<div class="blog-item-date-cont">
					<div class="blog-item-date"><?=date('d',$data->timework);?></div>
					<div class="blog-item-mounth"><?=CDates::getMounthMini(date('n',$data->timework));?></div>
				</div>
				<?php } ?>
				<div>
					<div class="blog-item-category-img">
						<img src="/images/icon-standart-post.png" alt="Ipsum" >
					</div>
				</div>
			</div>

			<div class="view view-first">				
				<?php if($img = $data->getPhotoNameUrl($data->id.'.jpg', $data->timeupdate)){ ?>						
					<a href="<?=$this->createUrl('/articles/view',array('alias'=>$data->alias));?>">
						<img src="<?=$img;?>" alt="<?php echo $data->{'name'.DMultilangHelper::getPrefix()};?>" style="width:100%" >					
						<div class="mask"></div>
					</a>
				<?php } ?>					
				
				<?php /*
				<div class="abs">
					<a href="<?=$this->createUrl('/articles/view',array('alias'=>$data->alias));?>" class="lightbox zoom info"></a><a href="<?=$this->createUrl('/articles/view',array('alias'=>$data->alias));?>" class="link info"></a>
				</div>	*/?>
			</div>
		</div>	
		<div class="blog-item-caption-container">
			<a class="a-invert" href="<?=$this->createUrl('/articles/view',array('alias'=>$data->alias));?>" ><span class="bold"><?php echo $data->{'name'.DMultilangHelper::getPrefix()};?></span> <?php echo $data->{'name2'.DMultilangHelper::getPrefix()};?></a>
			<div class="lp-item-container-border clearfix">
				<div class="blog-info-container ">
				<ul class="clearfix">				
					<li class="view"><?=CDates::getViewsText(@$data->views);?></li>
					<?php if($data->articlesAuthor && $data->articlesAuthor->author){ ?>
					<li class="author"><?=$data->articlesAuthor->author;?></li>
					<?php } ?>
					<?php /*
					<li class="author">Admin</li>
					<li class="comment">25 Comments</li>
					<li class="tag">Website Design - Responsive</li>
					*/?>
				</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="blog-item-text-container">
		<?php echo $data->{'anons'.DMultilangHelper::getPrefix()};?>
	</div>
	<div class="lp-r-m-container right">
		<a href="<?=$this->createUrl('/articles/view',array('alias'=>$data->alias));?>" class="button medium r-m-plus r-m-full">ЧИТАТЬ ДАЛЕЕ</a>
	</div>

</div>