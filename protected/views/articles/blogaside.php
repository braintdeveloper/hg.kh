<aside class="blogaside fl">
	
	<?php if($model->idbanner1){ ?>
		<?php $banner = Banners::model()->findByPk($model->idbanner1) ?>
		<?php if($banner){ ?>
			<a href="<?=$this->createUrl($banner->link);?>">
				<img src="<?=Banners::model()->getPhotoNameUrl($banner->id.'.'.Yii::app()->getLanguage().'.jpg', $banner->timeupdate);?>" alt="<?=$banner->{'name'.DMultilangHelper::getPrefix()};?>">
			</a>
		<?php }?>
	<?php }?>
	
	<br>
	<br>
	<br>
	
	<?php if($model->idbanner2){ ?>
		<?php $banner = Banners::model()->findByPk($model->idbanner2) ?>
		<?php if($banner){ ?>
			<a href="<?=$this->createUrl($banner->link);?>">
				<img src="<?=Banners::model()->getPhotoNameUrl($banner->id.'.'.Yii::app()->getLanguage().'.jpg', $banner->timeupdate);?>" alt="<?=$banner->{'name'.DMultilangHelper::getPrefix()};?>">
			</a>
		<?php }?>
	<?php }?>
	
</aside>