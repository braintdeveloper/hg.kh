<!-- PAGE TITLE -->
<div class="container m-bot-35 clearfix">
	<div class="sixteen columns">
		<div class="page-title-container clearfix">
			<h1 class="page-title">НОВОСТИ</h1>
		</div>	
	</div>
</div>	

</div>	<!-- Grey bg end -->	

<!-- CONTENT -->
<div class="container clearfix">
	<div class="twelwe columns m-bot-25">
		<!-- BLOG ITEM -->
		<div class="blog-item m-bot-35 clearfix">
			<div class="hover-item">
				<div class="clearfix">
					<div class="blog-item-date-inf-container left">
						<?php if($model->id != 1){ ?>
						<div class="blog-item-date-cont">
							<div class="blog-item-date"><?=date('d',$model->timework);?></div>
							<div class="blog-item-mounth"><?=CDates::getMounthMini(date('n',$model->timework));?></div>
						</div>
						<?php } ?>
						<div>
							<div class="blog-item-category-img">
								<img src="/images/icon-standart-post.png" alt="Ipsum" >
							</div>
						</div>
					</div>

					<div class="view view-first">
						<?php if($img = $model->getPhotoNameUrl($model->id.'.jpg', $model->timeupdate)){ ?>
							<img src="<?=$img;?>" alt="<?php echo $model->{'name'.DMultilangHelper::getPrefix()};?>" style="width:100%" >
						<?php } ?>	
						<?php /*
						<div class="mask"></div>							
						<div class="abs">
							<a href="<?=$img;?>" class="lightbox zoom info"></a>
							<a href="blog-single.html" class="link info"></a>
						</div>	
						*/?>
					</div>
				</div>	
				<div class="blog-item-caption-container">
					<div class="row">
						<div class="">
							<h1><span class="bold"><?php echo $model->{'name'.DMultilangHelper::getPrefix()};?></span></h1>
						</div>
						<div class="clear"></div>
						<div class="">
							<div class="blog-info-container">
								<ul class="clearfix">
									<li class="view in-blog"><?=CDates::getViewsText($model->views);?></li>
									<?php if($model->articlesAuthor && $model->articlesAuthor->author){ ?>
									<li class="author in-blog"><?=$model->articlesAuthor->author;?></li>
									<?php } ?>
								</ul>
							</div>
						</div>
						<div class="clearfix">
						<div class="caption-container-main">
							<div class="caption-text-container"><span class=""><?php echo $model->{'name2'.DMultilangHelper::getPrefix()};?></span></div>
							<div class="content-container-white caption-bg "></div>
						</div>
						</div>
						
					</div>

				</div>
			</div>
			<div class="blog-item-text-container clearfix">
				<?=$model->{'content'.DMultilangHelper::getPrefix()};?>
			</div>


		</div>
	</div>
</div>


<?php if($photoList){ ?>
<!-- CONTENT -->
<div class="container filter-portfolio clearfix">
	<ul id="portfolio" class="clearfix">
		<!-- PORTFOLIO ITEM -->
		<?php foreach($photoList as $data) { ?>				
		<li data-id="id-<?=$data->id;?>" data-type="category1" class="four columns m-bot-35">
			<div class="hover-item">
				<div class="view view-first">
					<?php if($img = Photos::model()->getPhotoUrl($data->id)){ ?>
					<a rel="photoList" href="<?=$img;?>">
						<img src="<?=$img;?>" alt="<?=$data->name;?>">
						<div class="mask"></div>	
					</a>
					<?php } ?>					
				</div>
				<?php /*<div class="lw-item-caption-container">
					<a class="a-invert" href="portfolio-single.html" >
						<div class="item-title-main-container clearfix">
							<div class="item-title-text-container">
								<span class="bold">Lorem</span> Ipsum
							</div>
						</div>
					</a>
					<div class="item-caption">web design</div>
				</div>*/?>
			</div>
		</li>
		<?php } ?>

	</ul>
</div>		

<?php } ?>


<!-- OUR PROJECTS End -->