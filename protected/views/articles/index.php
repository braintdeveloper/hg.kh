<!-- PAGE TITLE -->
<div class="container m-bot-35 clearfix">
	<div class="sixteen columns">
		<div class="page-title-container clearfix">
			<h1 class="page-title">НОВОСТИ</h1>
		</div>	
	</div>
</div>	

</div>	<!-- Grey bg end -->	

<!-- CONTENT -->
<div class="container clearfix">
	<div class="twelwe columns m-bot-25">
		<?php
			if(!$dataProvider->getData()){
				echo '<div class="styled-box iconed-box error bold">Ничего не найдено по заросу "'.$_GET['searcharticlesname'].'"</div>';
			} else if(isset($_GET['searcharticlesname'])){
				echo '<div class="styled-box iconed-box info bold">Результаты поиска по запросу "'.$_GET['searcharticlesname'].'"</div>';
			}
		?>
	
		<?php 
			$countPage = ceil($dataProvider->getTotalItemCount() / $dataProvider->pagination->getPageSize());
			$pagenavi = "<div class=\"wp-pagenavi myPagenavi\">";
			if($countPage>1){
				$pagenavi .= "<span class=\"pages\">Страница ".(@$_GET['Articles_page']? $_GET['Articles_page'] : 1)." из ".($countPage)."</span>";
			}
			$this->widget('zii.widgets.CListView',array(
				'dataProvider'=>$dataProvider,
				'ajaxUpdate'=>false,
				'itemView'=>'_view',
				'pagerCssClass'=>'pagination-1',
				'template'=>"{items}{pager}",
				'pager'=>array(
					'class' => 'bootstrap.widgets.TbPager',
					'htmlOptions'=>array(
	        	'class'=>'pagination-1-container'
	        ),  
					'maxButtonCount'=>5,
					// css class
	        /*'firstPageCssClass'=>'hidden',//default "first"
	        'lastPageCssClass'=>'hidden',//default "last"
	        'previousPageCssClass'=>'pag-prev',//default "previours"
	        'nextPageCssClass'=>'pag-next',//default "next"
	        //'internalPageCssClass'=>'pager_li',//default "page"
	        'selectedPageCssClass'=>'pag-current',//default "selected"
	        'hiddenPageCssClass'=>'hidden'//default "hidden"*/
				)
		)); ?>
	
	</div>

</div>