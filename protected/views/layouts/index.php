<!DOCTYPE html>
<html>
	<head>
		<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<meta charset=utf-8 >

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico">

		<!-- CSS begin -->


		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" >
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/skeleton.css" >

		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.fancybox-1.3.4.css"  >
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/switcher/style.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/layout/wide.css" id="layout">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/colors/myColor.css" id="colors">

		<!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie-warning.css" ><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style-ie.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ei8fix.css" ><![endif]-->

		<!-- flexslider slider CSS -->

		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/flexslider.css"  >

		<!--end flexslider slider CSS -->


		<!-- CSS end -->

		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script>
			var dir = '<?=Yii::app()->request->getBaseUrl(true).((Yii::app()->language!='ru') ? Yii::app()->language : '');?>';
		</script>

		<?=Yii::app()->params['adminSettings']['5'];?>
	</head>
	<body>

		<?=Yii::app()->params['adminSettings']['8'];?>

		<div id="wrap" class="boxed">
			<div class="grey-bg"> <!-- Grey bg  -->


				<!--[if lte IE 7]>
				<div id="ie-container">
				<div id="ie-cont-close">
				<a href='#' onclick='javascript&#058;this.parentNode.parentNode.style.display="none"; return false;'><img src='<?php echo Yii::app()->request->baseUrl; ?>/images/ie-warning-close.jpg' style='border: none;' alt='Close'></a>
				</div>
				<div id="ie-cont-content" >
				<div id="ie-cont-warning">
				<img src='<?php echo Yii::app()->request->baseUrl; ?>/images/ie-warning.jpg' alt='Warning!'>
				</div>
				<div id="ie-cont-text" >
				<div id="ie-text-bold">
				You are using an outdated browser
				</div>
				<div id="ie-text">
				For a better experience using this site, please upgrade to a modern web browser.
				</div>
				</div>
				<div id="ie-cont-brows" >
				<a href='http://www.firefox.com' target='_blank'><img src='<?php echo Yii::app()->request->baseUrl; ?>/images/ie-warning-firefox.jpg' alt='Download Firefox'></a>
				<a href='http://www.opera.com/download/' target='_blank'><img src='<?php echo Yii::app()->request->baseUrl; ?>/images/ie-warning-opera.jpg' alt='Download Opera'></a>
				<a href='http://www.apple.com/safari/download/' target='_blank'><img src='<?php echo Yii::app()->request->baseUrl; ?>/images/ie-warning-safari.jpg' alt='Download Safari'></a>
				<a href='http://www.google.com/chrome' target='_blank'><img src='<?php echo Yii::app()->request->baseUrl; ?>/images/ie-warning-chrome.jpg' alt='Download Google Chrome'></a>
				</div>
				</div>
				</div>
				<![endif]-->

				<!-- HEADER -->
				<header id="header" >
					<div class="container clearfix">
						<div class="sixteen columns <?=($this->uniqueid=='index' && $this->action->id=='index') ? 'header-position' : '';?> ">
							<div class="header-container m-top-30 clearfix">

								<div class="header-logo-container ">
									<div class="logo-container">
										<a href="<?=Yii::app()->getBaseUrl(true);?>" class="logo" rel="home" title="<?php echo Yii::app()->name; ?>">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-retina.png" alt="<?php echo Yii::app()->name; ?>" >
										</a>
									</div>
								</div>

								<div class="header-menu-container right">
									<!-- TOP MENU -->
									<nav id="main-nav">
										<ul class="sf-menu clearfix">
											<li><a href="<?=Yii::app()->getBaseUrl(true);?>">Главная</a></li>
											<li><a href="<?=$this->createUrl('/team');?>">Команда</a></li>
											<li><a href="<?=$this->createUrl('/articles');?>">Новости</a></li>
											<li><a href="<?=$this->createUrl('/video');?>">Видео</a></li>
											<li><a href="<?=$this->createUrl('/audio');?>">Аудио</a></li>
											<?php /*<li><a href="javascript:void(0);" style="cursor: default;">Медиа</a>
												<ul>
													<li><a href="<?=$this->createUrl('/video');?>">Видео</a></li>
													<li><a href="<?=$this->createUrl('/audio');?>">Аудио</a></li>
												</ul>
											</li>*/?>
											<li><a href="<?=$this->createUrl('/contacts');?>">Контакты</a></li>
										</ul>
									</nav>

									<div class="search-container ">
										<form action="#" class="search-form">
											<input type="text" name="name" class="search-text" value="<?php if(isset($_GET['searcharticlesname'])) echo $_GET['searcharticlesname'];?>" >
											<input type="submit" value="" class="search-submit" name="submit" onclick="searchForm();return false;">
										</form>
										<script>
    									function searchForm(){
												var searchInput = jQuery('.search-form input[name="name"]').val();
												if(searchInput==''){
													alert('Ошибка! Вы забыли ввести фразу для поиска.');
													return false;
												}
												location.href = '<?=$this->createUrl('/articles/search')?>'+'/'+searchInput;
    									}
										</script>

									</div>

								</div>

							</div>
						</div>
					</div>

				</header>


				<?php echo $content; ?>
				<?php //#Flash messages Box
					$flashMessages = Yii::app()->user->getFlashes();
					if ($flashMessages) {
						echo '<ul class="flashes">';
						foreach($flashMessages as $key => $message) {
							echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
						}
						echo '</ul>';
					}
				?>

			<?php //</div>	<!-- Grey bg end -->  - Gone to $content;?>


			<!-- FOOTER -->
			<?php $copyright = Textpages::model()->findByPk(8); ?>
			<?php //echo $copyright->{'content'.DMultilangHelper::getPrefix()};?>

			<footer>
				<div class="footer-content-bg">
					<div class="container clearfix">
						<div class="one-third column">
							<div class="footer-social-text-container">
								<p class="title-font-24">ПОЛЕЗНЫЕ <strong>РЕСУРСЫ</strong></p>
								<p class="title-font-12">СОЦ. СЕТИ И НАШИ ДРУЗЬЯ</p>
							</div>
							<div class="footer-social-links-container">
								<ul class="styled-list style-1 clearfix friends-link">
									<li><a target="_blank" title="Группа вконтакте HOLY GENERATION" href="http://vk.com/holygeneration">Наша группа Вконтакте</a></li>
									<li><a target="_blank" title="Youtube HOLY GENERATION" href="https://www.youtube.com/user/hgkharkov/">Youtube HOLY GENERATION</a></li>
									<li><a target="_blank" title="Instagram" href="https://www.instagram.com/holy_generation/">Instagram</a></li>
									<li><a target="_blank" title="Церковь Благая Весть" href="http://good-news.in/">Церковь Благая Весть</a></li>
									<?php /*<li><a class="skype-link" target="_blank" title="Skype" href="#"></a></li>
									<li><a class="googleplus-link" target="_blank" title="googleplus" href="#"></a></li>
									<li><a class="twitter-link" target="_blank" title="Twitter" href="#"></a></li>
									<li><a class="flickr-link" target="_blank" title="Flickr" href="#"></a></li>
									<li><a class="vimeo-link" target="_blank" title="Vimeo" href="#"></a></li>
									<li><a class="linkedin-link" target="_blank" title="linkedin" href="#"></a></li>
									<li><a class="pintrest-link" target="_blank" title="pintrest" href="#"></a></li>*/?>
								</ul>
							</div>
						</div>

						<div class="one-third column">
							<?php $latestNews=array(); ?>
							<?php $articles = Articles::model()->findAll('status=1 ORDER BY timecreate DESC LIMIT 3');?>
							<?php foreach($articles as $data){
								$latestNews[$data->timecreate] = array(
									'name' => $data->name_ru,
									'url' => $this->createUrl('articles/view', array('alias'=>$data->alias)),
									'timecreate' => $data->timecreate,
									'iconClass' => 'standart-post',
								);
							} ?>

							<?php $videos = Video::model()->findAll('status=1 ORDER BY sort ASC LIMIT 3');?>
							<?php foreach($videos as $data){
								$latestNews[$data->timecreate] = array(
									'name' => $data->name,
									'url' => $this->createUrl('/video#show_video='.$data->youtubeurl),
									'timecreate' => $data->timecreate,
									'iconClass' => 'video-post',
								);
							} ?>
							<?php krsort($latestNews);?>
							<?php $latestNews = array_slice($latestNews, 0, 3);?>
							<h3 class="caption footer-block">ПОСЛЕДНЕЕ ИЗ <span class="bold">НОВОСТЕЙ</span></h3>
							<ul class="latest-post">
								<?php foreach($latestNews as $data) { ?>
								<li class="<?=$data['iconClass'];?>">
									<h4 class="title-post-footer"><a href="<?=$data['url'];?>"><?=$data['name'];?></a></h4>
								</li>
								<?php } ?>
							</ul>
						</div>

						<div class="one-third column ">
							<h3 class="caption footer-block">НАШИ <span class="bold">КОНТАКТЫ</span></h3>
							<ul class="footer-contact-info">
								<li class="footer-loc">
									Украина, г.Харьков, пр. Московский, 94 ДК ХЭМЗ 2-й этаж
								</li>
								<li class="footer-phone">
									<p style="margin-top: 6px;">
									(057) 716 93-11
									<br><?php /*(093) 579 43-83*/?>
									</p>
								</li>
								<li class="footer-mail">
									<p class="m-top-10">
										<a href="mailto:mail@hg.kh.ua">mail@hg.kh.ua</a>
									</p><br>
								</li>
							</ul>

						</div>
					</div>
				</div>
				<div class="footer-copyright-bg">
					<div class="container ">
						<div class="sixteen columns clearfix">
							<div class="footer-menu-container">
								<nav class="clearfix" id="footer-nav">
									<ul class="footer-menu">
										<li class="current"><a href="<?=Yii::app()->getBaseUrl(true);?>">Гланая</a></li>
										<li><a href="<?=$this->createUrl('/team');?>">Команда</a></li>
										<li><a href="<?=$this->createUrl('/articles');?>">Новости</a></li>
										<li><a href="<?=$this->createUrl('/video');?>">Видео</a></li>
										<?php /*<li><a href="<?=$this->createUrl('/audio');?>">Аудио</a></li>*/?>
										<li><a href="<?=$this->createUrl('/contacts');?>">Контакты</a></li>
									</ul>
								</nav>
							</div>
							<div class="footer-copyright-container right-text">
								<a class="author" href="http://brainit.in.ua">Создание сайта - brainit.in.ua</a>
							</div>
						</div>

					</div>
				</div>
			</footer>
			<p id="back-top">
				<a href="#top" title="Back to Top"><span></span></a>
			</p>
		</div><!-- End wrap -->
		<!-- JS begin -->

		<?php /*<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.8.3.min.js"></script>*/?>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/superfish.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>
		<!-- Flex Slider  -->
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.flexslider.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/flex-slider.js"></script>
		<!-- end Flex Slider -->
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jcarousel.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jQuery.BlackAndWhite.min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jflickrfeed.min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.quicksand.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-cookie.js"></script>


		<!-- JS end -->

		<?=Yii::app()->params['adminSettings']['5'];?>

	</body>
</html>