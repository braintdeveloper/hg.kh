<?php
	//selectBox
	Yii::app()->getClientScript()->registerCssFile('/css/customSelectBox.css');
	Yii::app()->getClientScript()->registerScriptFile('/js/SelectBox.js');
	
	//gMap
	Yii::app()->getClientScript()->registerScriptFile('https://maps.google.com/maps/api/js?sensor=false&language=ru');
	Yii::app()->getClientScript()->registerScriptFile('/js/map.js');
	Yii::app()->getClientScript()->registerScriptFile('/js/infobox.js');
?>
<script>
	textRegionLabel = '<?php echo Yii::t('View', 'Выберите область'); ?>';
	textCityLabel = '<?php echo Yii::t('View', 'Выберите город'); ?>';
	textTradenetLabel = '<?php echo Yii::t('View', 'Выберите аптеку'); ?>';
</script>
<style>
.bubble {
	color: #2a99ad;
	width: 230px;
	padding: 15px 10px 11px;
	border: 1px solid #fff;
	border-radius: 10px;
	background: #fff;
	position: absolute;
	font-size: 18px;
	z-index: 10;
}
.bubble:after {
	/*width: 37px;
	height: 48px;
	background: url(../images/corner.png) no-repeat;
	position: absolute;
	content: '';
	bottom: -48px;
	left: 50px;*/
	content: '';
	position: absolute;
	width: 0px;
	height: 0px;
	border-style: solid;
	border-width: 50px 15px 0 15px;
	border-color: #fff transparent transparent transparent;
	left: 35px;
	bottom: -50px;
}
</style>

<?php	$this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>array(
			Yii::t('Text', 'Где купить?'),
		),
		'separator'=>' → '
));?>
<section class="inner">
	<section class="map" id="wheretobuy-map">
		<h1><?php echo Yii::t('Text', 'Места продажи'); ?></h1>
		<div class="selectblock clearfix">
			
			<?php $regions = GmapRegions::model()->findAll();?>
						
			<select name="region" id="region" class="custom">
				<option value="0"><?php echo Yii::t('Text', 'Выберите область'); ?></option>
				<?php foreach($regions as $row) { ?>
				<option value="<?=$row->id; ?>"><?=$row->{'region'.DMultilangHelper::getPrefix()}; ?></option>
				<?php } ?>
			</select>
			<select name="city" id="city" class="custom">
				<option value="0"><?php echo Yii::t('Text', 'Выберите город'); ?></option>
			</select>
			<select name="tradenet" id="tradenet" class="custom">
				<option value="0"><?php echo Yii::t('Text', 'Выберите аптеку'); ?></option>
			</select>
		</div>
		<div class="mapblock" id="mapblock">
		</div>
		<div class="tradeaddrblock clearfix">
			<?php /*<address>Київ, вул.Ватутіна, 2Т<br> 1608, Аптека «03»</address>
			<address>Київ, вул.Ватутіна, 2Т<br> 1608, Аптека «03»</address>*/?>
		</div>
	</section>
</section>