<?php $model = ProductsTypes::model()->findAll();?>

<?php if($model){ ?>
	<div id="myImageFlow" class="imageflow">
		<?php foreach($model as $row){ ?>
			<img src="<?=ProductsTypes::model()->getPhotoUrl($row->id, $row->timeupdate);?>" alt="">
		<?php }?>
	</div>
	
	<div class="text_gallery_block">		
		<div class="text_gallery_block_h2">
			<?php foreach($model as $key => $row){ ?>
			<h3 id="type_<?=$key+1?>" class="head_block" <?php if($key>0){echo'style="display: none;"';} ?>><?=$row->{'name'.DMultilangHelper::getPrefix()}?></h3>
			<?php } ?>
		</div>
		
		<?php foreach($model as $key => $row){ ?>
		<div id="bottle_<?=$key+1?>" class="price text_bottle" <?php if($key>0){echo'style="display: none;"';} ?>>
			<span class="recommend"><?php echo Yii::t('Text', 'Рекомендуемая цена:'); ?></span>
			<span class="int"><?=$row->price;?></span>
			<span class="float">
				<?=$row->cents;?>
				<span class="hrn"><?php echo Yii::t('Text', 'грн.'); ?></span>
			</span>
		</div>
		<?php } ?>		
	</div>
	
<?php } ?>