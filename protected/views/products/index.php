<?php	$this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>array(
			Yii::t('Text','Продукция'),
		),
		'separator'=>' → '
	));?>
<section class="inner">
	<section class="allprod">
		
		<?=$meta->{'content'.DMultilangHelper::getPrefix()};?>		
		
		<?php $this->renderPartial('banners')?>
		
		<h1><?php echo Yii::t('Text', 'Линейки продукций:'); ?></h1>
		<div class="productlines">
			<?php foreach($dataProvider->getData() as $model){ ?>			
			<div class="tac">
				<img src="<?=$model->getPhotoNameUrl($model->id.'-2.jpg', $model->timeupdate)?>" alt="<?=$model->{'anonsname'.DMultilangHelper::getPrefix()};?>" width="110" onclick="window.location.href='<?=$this->createUrl('/products/view', array('alias'=>$model->alias));?>'" />
				<h3><?=$model->{'anonsname'.DMultilangHelper::getPrefix()};?></h3>
				<p><?=$model->{'anonstext'.DMultilangHelper::getPrefix()};?></p>
				<a href="<?=$this->createUrl('/products/view', array('alias'=>$model->alias));?>"><?php echo Yii::t('Text', 'Подробнее'); ?></a>
			</div>
			<?php } ?>
		</div>
	</section>
</section>