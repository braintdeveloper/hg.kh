<?php
	Yii::app()->getClientScript()->registerScriptFile('/js/imageflow.js');
	Yii::app()->getClientScript()->registerCssFile('/css/imageflow.packed.css');	
?>

<?php	$this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>array(
			Yii::t('Text','Продукция') => $this->createUrl('/products/index'),
			$model->{'name'.DMultilangHelper::getPrefix()},
		),
		'separator'=>' → '
	));?>
<section class="inner">
	<section class="productinfo fl">
		<h1><?=$model->{'name'.DMultilangHelper::getPrefix()};?></h1>
		<?php if($model->sample){ ?>
		<a href="<?=$this->createUrl('/products/sampling');?>" class="getsample"><?php echo Yii::t('Text', 'Заказать семпл'); ?></a>
		<?php } ?>
		<?=$model->{'content'.DMultilangHelper::getPrefix()};?>	
	</section>
	<aside class="productaside fl">
		
		<?php if($model->id==1){ ?>
		<?php $this->renderPartial('productsTypes')?>	
		<?php } else { ?>
		<?php $this->renderPartial('productTypesOther', array('model'=>$model))?>			
		<?php } ?>
		
		<?php if($model->productsToUsed0){ ?>
		<section class="usedon">
			<h2><?php echo Yii::t('Text', 'Используют при:'); ?></h2>
			<?php foreach($model->productsToUsed0 as $row){ ?>
			<article>
				<img src="<?=ProductsUsed::model()->getPhotoUrl($row->id, $row->timeupdate)?>" alt="<?=$row->{'name'.DMultilangHelper::getPrefix()};?>"/>
				<p><?=$row->{'name'.DMultilangHelper::getPrefix()};?></p>
			</article>
			<?php } ?>
		</section>
		<?php } ?>
		
	</aside>
	
	<?php $this->renderPartial('banners')?>	
	
	<?php if($otherProducts){ ?>
	<div class="otherproducts">
		<h1><?php echo Yii::t('Text', 'Продукция из других линий:'); ?></h1>
		<div class="otherproductsholder">
			<?php foreach((array)$otherProducts as $row){ ?>			
			<a href="<?=$this->createUrl('/products/view', array('alias'=>$row->alias));?>">
				<img src="<?=$model->getPhotoNameUrl($row->id.'-2.jpg', $row->timeupdate)?>" alt="<?=$row->{'anonsname'.DMultilangHelper::getPrefix()};?>" width="111"  class="fl"/>
				<span class="overfl">
					<span class="textlink"><?=$row->{'anonsname'.DMultilangHelper::getPrefix()};?></span>
					<span class="price">
						<span class="recommend"><?php echo Yii::t('Text', 'Рекомендуемая цена:'); ?></span>
							<span class="int"><?=$row->price;?></span>
							<span class="float">
								<?=$row->cents;?>
								<span class="hrn"><?php echo Yii::t('Text', 'грн.'); ?></span>
						</span>
					</span>
				</span>
			</a>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
	
	
</section>