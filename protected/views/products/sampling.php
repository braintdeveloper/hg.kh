<?php
	//selectBox
	Yii::app()->getClientScript()->registerCssFile('/css/customSelectBox.css');
	Yii::app()->getClientScript()->registerScriptFile('/js/SelectBox.js');
	
	//gMap
	Yii::app()->getClientScript()->registerScriptFile('https://maps.google.com/maps/api/js?sensor=false&language=ru');
	Yii::app()->getClientScript()->registerScriptFile('/js/map.js');
	Yii::app()->getClientScript()->registerScriptFile('/js/infobox.js');
?>
<?php	$this->widget('zii.widgets.CBreadcrumbs', array(
	'links'=>array(
		Yii::t('Text','Продукция') => $this->createUrl('/products/index'),
		Yii::t('Text','breadcrums Семплинг'),
	),
	'separator'=>' → '
));?>
<section class="inner">
	<section class="sampling">		
		
		<h1><?=$meta->{'name'.DMultilangHelper::getPrefix()};?></h1>		
		<?=$meta->{'content'.DMultilangHelper::getPrefix()};?>
	
	
	
		<?php 
			$sampling = Settings::model()->find('name="sampling"');
			$samplingValue = $sampling->value - UsersSampling::model()->count(); 
		?>
		<?php if($samplingValue > 0){ ?>
		<div class="jsShowFoemBox">
			<h1><?php echo Yii::t('Text', 'ЗАПОЛНИТЕ ДАННЫЕ И ПОЛУЧИТЕ<br>БЕСПЛАТНЫЙ пластырь COMPEED®*'); ?></h1>
						
			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
				'id'=>'sampling-form',
				'enableAjaxValidation'=>false,
				'action'=>$this->createUrl('/products/sampling'),
				'htmlOptions'=>array('class'=>'sampleform clearfix'),
			)); ?>
			<?php /*<form action="" class="sampleform clearfix">*/?>
				
				<?php /*<div class="clear"><?php echo $form->errorSummary($model); ?></div>*/?>
				
				<div class="<?php if($form->error($model, 'fio')) { echo 'error';}?>">
					<p><?=$model->getAttributeLabel('fio');?></p>
					<?php echo $form->textField($model,'fio',array('id'=>'flname','maxlength'=>255)); ?>
					<?php if($form->error($model, 'fio')){ ?>
						<div class="baloonblue"><?php echo $form->error($model, 'fio'); ?></div>
					<?php } ?>
				</div>
				
				<div class="<?php if($form->error($model, 'email')) { echo 'error';}?>">
					<p><?=$model->getAttributeLabel('email');?></p>
					<?php echo $form->textField($model,'email',array('id'=>'email','maxlength'=>255)); ?>
					<?php if($form->error($model, 'email')){ ?>
						<div class="baloonred"><?php echo $form->error($model, 'email'); ?></div>
					<?php } ?>
				</div>
				
				<div class="<?php if($form->error($model, 'phone')) { echo 'error';}?>">
					<p><?=$model->getAttributeLabel('phone');?></p>
					<select name="UsersSampling[phonecode]" id="phonecode" class="custom">
						<?php foreach($model->phoneCode as $v){?>
						<option <?php if(@$_POST['UsersSampling']['phonecode']==$v){ echo 'selected';}?> value="<?=$v;?>"><?=$v;?></option>
						<?php } ?>
					</select>
					<input id="phone" maxlength="7" name="UsersSampling[phonecode2]" type="text" value="<?=@$_POST['UsersSampling']['phonecode2']?>">
					<?php if($form->error($model, 'phone')){ ?>
						<div class="baloonred"><?php echo $form->error($model, 'phone'),$form->error($model, 'phonecode'); ?></div>
					<?php } ?>
				</div>
				
				<div class="<?php if($form->error($model, 'region')) {echo 'error';}?>">
					<p><?=$model->getAttributeLabel('region');?></p>
					<?php echo $form->textField($model,'region',array('id'=>'sregion','maxlength'=>255)); ?>
					<?php if($form->error($model, 'region')){ ?>
						<div class="baloonblue"><?php echo $form->error($model, 'region'); ?></div>
					<?php } ?>
				</div>
				
				<div class="<?php if($form->error($model, 'city')) {echo 'error';}?>">
					<p><?=$model->getAttributeLabel('city');?></p>
					<?php echo $form->textField($model,'city',array('id'=>'scity','maxlength'=>255)); ?>
					<?php if($form->error($model, 'city')){ ?>
						<div class="baloonblue"><?php echo $form->error($model, 'city'); ?></div>
					<?php } ?>
				</div>
				
				<div class="<?php if($form->error($model, 'street')) {echo 'error';}?>">
					<p><?=$model->getAttributeLabel('street');?></p>
					<?php echo $form->textField($model,'street',array('id'=>'street','maxlength'=>255)); ?>
					<?php if($form->error($model, 'street')){ ?>
						<div class="baloonblue"><?php echo $form->error($model, 'street'); ?></div>
					<?php } ?>
				</div>
				
				<div class="<?php if($form->error($model, 'house')) {echo 'error';}?>">
					<p><?=$model->getAttributeLabel('house');?></p>
					<?php echo $form->textField($model,'house',array('id'=>'house','maxlength'=>20)); ?>
					<?php if($form->error($model, 'house')){ ?>
						<div class="baloonblue"><?php echo $form->error($model, 'house'); ?></div>
					<?php } ?>
				</div>
				
				<div class="<?php if($form->error($model, 'plate')) {echo 'error';}?>">
					<p><?=$model->getAttributeLabel('plate');?></p>
					<?php echo $form->textField($model,'plate',array('id'=>'apartment','maxlength'=>20)); ?>
					<?php if($form->error($model, 'plate')){ ?>
						<div class="baloonblue"><?php echo $form->error($model, 'plate'); ?></div>
					<?php } ?>
				</div>
				
				<div class="<?php if($form->error($model, 'postindex')) {echo 'error';}?>">
					<p><?=$model->getAttributeLabel('postindex');?></p>
					<?php echo $form->textField($model,'postindex',array('id'=>'zipcode','maxlength'=>6)); ?>
					<?php if($form->error($model, 'postindex')){ ?>
						<div class="baloonblue"><?php echo $form->error($model, 'postindex'); ?></div>
					<?php } ?>
				</div>
				
			<div class="importantblock fl">
				<p><?php echo Yii::t('Text', 'ВАЖНО! Заполняйте Ваши настоящие данные – они будут использованы исключительно при оформлении доставки на ближайшее отделение УКРПОЧТЫ по Вашему адресу'); ?></p>
				<label class="checkboxLabel <?php if($form->error($model, 'agree')) {echo 'error';}?>">
					<?php echo $form->checkBox($model,'agree',array('class'=>'fl')); ?>
					<span class="overfl" style="display: block;"><?php echo Yii::t('Text', 'Согласен с'); ?> <a href="<?=$this->createUrl('/textpages/view', array('alias'=>'privacy-policy'));?>"><?php echo Yii::t('Text', 'Правилами Конфиденциальности'); ?></a> <?php echo Yii::t('Text', 'и'); ?> <a href="<?=$this->createUrl('/textpages/view', array('alias'=>'terms-of-use'));?>"><?php echo Yii::t('Text', 'Условиями Использования'); ?></span></a>
				</label>
				<div class="captcha <?php if($form->error($model, 'verifyCode')) {echo 'error';}?>">
					<p><?php echo Yii::t('Text', 'Решите пример:'); ?></p>
					<div>
						<?php if(CCaptcha::checkRequirements()) { ?>
						  <?=CHtml::activeLabelEx($model, 'verifyCode');?>
						  <?php $this->widget('CCaptcha', array('clickableImage'=>true,'showRefreshButton'=>false));?>
						  <?=CHtml::activeTextField($model, 'verifyCode');?>
						<?php } ?>
					</div>
				</div>
				<a href="<?=$this->createUrl('/textpages/view/', array('alias'=>'special-terms'));?>" class="greybtn"><?php echo Yii::t('Text', 'Правила акции'); ?></a>
				<a href="" class="bluebtn" onclick="getSampling();return false;"><?php echo Yii::t('Text', 'Заказать'); ?></a>
			</div>
			<?php $this->endWidget(); ?>
			
			<div class="samplesleft fl">
				<span><?php echo Yii::t('Text', 'Осталось'); ?></span>
				<?php	foreach((array)str_split($samplingValue) as $v){
					echo '<div class="digit">'.$v.'</div>';
				} ?>
				<span><?php echo Yii::t('Text', 'ОБРАЗЦОВ*'); ?></span>
			</div>
			<div class="clear"></div>
			<div class="remarks">
				<p><?php echo Yii::t('Text', '1. Под бесплатным пластырем подразумевается пластырь COMPEED® от влажных мозолей средний 1шт.'); ?></p>
				<p><?php echo Yii::t('Text', '2. Клиническое испытание 7151071.TK «Колопласт Компид моно-гидроколоидний рецепт»'); ?></p>
				<p><?php echo Yii::t('Text', '3. Клиническое испытание «Тест EAME MRD July07 использование пластырей COMPEED»'); ?></p>
				<p><?php echo Yii::t('Text', '4. Согласно информации на упаковке'); ?></p>
			</div>
		</div>
		<div class="gotsample" style="display: none;">
			<p><?php echo Yii::t('Text', 'ПОЗДРАВЛЯЕМ!'); ?></p>
			<span><?php echo Yii::t('Text', 'На ваш e-mail отправлено подтверждение заказа семпла и детали по его получению. <br>Будьте внимательны – проверьте СПАМ фильтры.'); ?></span>
		</div>
		
		<?php } else { ?>
			<h1><?php echo Yii::t('Text', 'В данный момент количество семплов ограничено.'); ?></h1>
		<?php } ?>
		
	</section>
</section>