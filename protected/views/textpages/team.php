<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
				<h1 class="page-title">Команда</h1>
			</div>	
		</div>
	</div>	
	
</div>	<!-- Grey bg end -->	

<!-- CONTENT -->
	<div class="container m-bot-35 clearfix">
		
					<div class="sixteen columns">
						<div class="img-about">
							<img class="" alt="about" src="images/team.gif">
						</div>
					</div>
					<div class="sixteen columns">						
						<div class="caption-container-main m-bot-30">
							<div class="caption-text-container"><?php echo $model->{'name'.DMultilangHelper::getPrefix()};?></div>
							<div class="content-container-white caption-bg "></div>
						</div>
						<?php echo $model->{'content'.DMultilangHelper::getPrefix()};?>
					</div>
			
	</div>
	<?php /*<div class="container m-bot-35 clearfix">

				<div class="row">
					<div class="eight columns">
						<div class="caption-container-main m-bot-30">
							<div class="caption-text-container">WHO WE ARE?</div>
							<div class="content-container-white caption-bg "></div>
						</div>
						
							<ul id="toggle-view">
								<li>
									<h3 class="ui-accordion-header"><span class="link"></span>Lorem ipsum</h3>
									<div class="panel">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus 
										neque, id pulvinar odio lorem non turpis. Nullam sit amet enim.</p>
									</div>
								</li>
								<li>
									<h3 class="ui-accordion-header"><span class="link"></span>Vestilum pulvinar</h3>
									<div class="panel">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus 
										neque, id pulvinar odio lorem non turpis. Nullam sit amet enim.</p>
									</div>
								</li>
								<li>
									<h3 class="ui-accordion-header"><span class="link"></span>Donec sedin</h3>
									<div class="panel">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus 
										neque, id pulvinar odio lorem non turpis. Nullam sit amet enim.</p>
									</div>
								</li>
								<li>
									<h3 class="ui-accordion-header"><span class="link"></span>Morbi commodo</h3>
									<div class="panel">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus 
										neque, id pulvinar odio lorem non turpis. Nullam sit amet enim.</p>
									</div>
								</li>
							</ul>
						
					</div>
					<div class="eight columns">
						<div class="caption-container-main m-bot-30">
							<div class="caption-text-container">OUR SKILLS</div>
							<div class="content-container-white caption-bg "></div>
						</div>
						<div id="skill-bars">
							<div class="skill-bar"><div class="skill-bar-content" data-percentage="80"></div><span class="skill-title">WEB DESIGN 80%</span></div>
							<div class="skill-bar"><div class="skill-bar-content" data-percentage="90"></div><span class="skill-title">HTML / CSS 90%</span></div>
							<div class="skill-bar"><div class="skill-bar-content" data-percentage="85"></div><span class="skill-title">WORDPRESS 85%</span></div>
							<div class="skill-bar"><div class="skill-bar-content" data-percentage="75"></div><span class="skill-title">SEO 75%</span></div>
						</div>
					</div>
				</div>
			

	</div>*/?>