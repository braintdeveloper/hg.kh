<!-- PAGE TITLE -->
<div class="container m-bot-35 clearfix">
	<div class="sixteen columns">
		<div class="page-title-container clearfix">
			<h1 class="page-title"><?php echo $model->{'name'.DMultilangHelper::getPrefix()};?></h1>
		</div>	
	</div>
</div>	

</div>	<!-- Grey bg end -->	

<!-- CONTENT -->
<div class="container clearfix">
	<div class="twelwe columns m-bot-25">
		<!-- BLOG ITEM -->
		<div class="blog-item">
			<div class="container m-bot-35 clearfix">
				<?php echo $model->{'content'.DMultilangHelper::getPrefix()};?>
			</div>
		</div>
	</div>
</div>



<!-- OUR PROJECTS End -->