<!-- PAGE TITLE -->
<div class="container m-bot-35 clearfix">
	<div class="sixteen columns">
		<div class="page-title-container clearfix">
			<h1 class="page-title">Видео</h1>
			<?php $category = VideoCategory::model()->findAll(); ?>
			<?php if($category){ ?>
			<ul id="filter"> 
				<li class="current all"><a href="#">All</a></li> 
				<?php foreach($category as $data){ ?>
				<li class="category<?=$data->id;?>"><a href="#"><?=$data->{'category'.DMultilangHelper::getPrefix()};?></a></li> 
				<?php } ?>
			</ul>
			<?php } ?>
		</div>
	</div>
</div>		

</div>	<!-- Grey bg end -->	




<?php $video = Video::model()->findAll('status=1 ORDER BY sort asc'); ?>

<?php if($video) { ?>
<!-- CONTENT -->
<div class="container filter-portfolio clearfix">
	<ul id="portfolio" class="clearfix">
		
		<?php foreach($video as $data){ ?>
		<!-- PORTFOLIO ITEM -->
		<li data-id="id-<?=$data->id;?>" data-type="category<?=$data->id_category;?>" class="four columns m-bot-35">
			<div class="hover-item">
				<div class="view view-first">
					<img src="http://img.youtube.com/vi/<?=$data->youtubeurl;?>/mqdefault.jpg" alt="<?=$data->name;?>">
					<?php /*<img src="http://img.youtube.com/vi/<?=$data->youtubeurl;?>/maxresdefault.jpg" alt="Ipsum" >*/?>
					<div class="mask" onclick="showVideo('<?=$data->youtubeurl;?>');return false;" title="Показать видео: <?=$data->name;?>" style="cursor:pointer;"></div>	
					<div class="abs">
						<?php /*<a onclick="showVideo('<?=$data->youtubeurl;?>');return false;" class="zoom info"></a>*/?>
						<a href="#" onclick="showVideoLink('<?=Yii::app()->getBaseUrl(true)?>/video#show_video=<?=$data->youtubeurl;?>','<?=$data->name;?>');return false;" class="link info"></a>
					</div>	
				</div>
				<div class="lw-item-caption-container">
					<a class="a-invert" href="#" onclick="showVideo('<?=$data->youtubeurl;?>');return false;" >
						<div class="item-title-main-container clearfix">
							<div class="item-title-text-container">
								<span class="bold" data-youtubeurl="<?=$data->youtubeurl;?>"><?=$data->name;?></span>
							</div>
						</div>
					</a>
					<div class="item-caption"><?=$data->video0->category_ru;?></div>
				</div>
			</div>
		</li>
		<?php } ?>


	</ul>
</div>	


<div class="popup popupVideo"></div>
<div class="popup popupVideoLink"></div>
<?php } ?>


<?php /*<!-- PAGINATION -->
<div class="container clearfix">
	<div class="pagination-1-container sixteen columns">
		<ul class="pagination-1">
			<li>
				<a class="pag-prev" href="#"></a>
			</li>
			<li>
				<a class="pag-current" href="#">1</a>
			</li>
			<li>
				<a href="#">2</a>
			</li>
			<li>
				<a href="#">3</a>
			</li>
			<li>
				<a class="pag-next" href="#"></a>
			</li>
		</ul>
	</div>
</div>*/?>