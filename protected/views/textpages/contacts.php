<!-- PAGE TITLE -->
<div class="container  clearfix">
	<div class="sixteen columns">
		<div class="page-title-container clearfix">
			<h1 class="page-title">КОНТАКТЫ</h1>
		</div>	
	</div>
</div>	

</div>	<!-- Grey bg end -->	

<!-- CONTENT -->
<div class="clearfix">
	<div class="m-bot-10">
		<!-- Google Maps -->
		<section class="google-map-container">
			<div id="googlemaps" class="google-map"></div>
		</section>
		<!-- Google Maps / End -->
	</div>
</div>
<!-- CONTACT FORM-->
<div class="container clearfix">
	<div class="twelve columns  m-bot-35">
		<div class="caption-container-main m-bot-30">
			<div class="caption-text-container"><span class="bold">ОТПРАВТЕ</span> НАМ СООБЩЕНИЕ</div>
			<div class="content-container-white caption-bg "></div>
		</div>


		<div class="contact-form-container">				
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'contact-form',
				'enableAjaxValidation'=>true,
  			'action'=>$this->createUrl('/contacts'),
  			/*'clientOptions'=>array(
    			'validateOnSubmit'=>true,
    		),*/    		
				'htmlOptions'=>array(
					'class'=>'clearfix'
				)
			)); ?>
				<fieldset class="field-1-3 left">
					<label><?=$modelContactsForm->getAttributeLabel('name');?></label>
					<!--<input type="text" name="name" id="Myname" onblur="if(this.value=='')this.value='Your name...';" onfocus="if(this.value=='Your name...')this.value='';" value="Your name..." class="text requiredField m-bot-20" >-->
					<?php echo $form->textField($modelContactsForm,'name', array(
						'class'=>'text requiredField name m-bot-20',
						'onblur'=>"if(this.value=='')this.value='{$modelContactsForm->getAttributeLabel('name')}';",
						'onfocus'=>"if(this.value=='{$modelContactsForm->getAttributeLabel('name')}')this.value='';",
						'value'=>$modelContactsForm->getAttributeLabel('name'),
					)); ?>
					<?php echo $form->error($modelContactsForm,'name',array('class' => 'styled-box iconed-box error')); ?>
					
				</fieldset >
				<fieldset class="field-1-3 left">
					<label><?=$modelContactsForm->getAttributeLabel('email');?></label>	
					<!--<input type="text" name="email" id="myemail"   onblur="if(this.value=='')this.value='Your email...';" onfocus="if(this.value=='Your email...')this.value='';" value="Your email..."  class="text requiredField email m-bot-20" >-->
					<?php echo $form->textField($modelContactsForm,'email', array(
						'class'=>'text requiredField email m-bot-20',
						'onblur'=>"if(this.value=='')this.value='{$modelContactsForm->getAttributeLabel('email')}';",
						'onfocus'=>"if(this.value=='{$modelContactsForm->getAttributeLabel('email')}')this.value='';",
						'value'=>$modelContactsForm->getAttributeLabel('email'),
					)); ?>
					<?php echo $form->error($modelContactsForm,'email',array('class' => 'styled-box iconed-box error')); ?>
				</fieldset>
				<fieldset class="field-1-3 left">
					<label><?=$modelContactsForm->getAttributeLabel('subject');?></label>	
					<!--<input type="text" name="subject" id="mySubject"  onblur="if(this.value=='')this.value='Subject...';" onfocus="if(this.value=='Subject...')this.value='';" value="Subject..." class="text requiredField subject m-bot-20" >-->
					<?php echo $form->textField($modelContactsForm,'subject', array(
						'class'=>'text subject m-bot-20',
						'onblur'=>"if(this.value=='')this.value='{$modelContactsForm->getAttributeLabel('subject')}';",
						'onfocus'=>"if(this.value=='{$modelContactsForm->getAttributeLabel('subject')}')this.value='';",
						'value'=>$modelContactsForm->getAttributeLabel('subject'),
					)); ?>
					<?php echo $form->error($modelContactsForm,'subject',array('class' => 'styled-box iconed-box error')); ?>
				</fieldset>	
				<fieldset class="field-1-1 left">
					<label><?=$modelContactsForm->getAttributeLabel('message');?></label>
					<?php echo $form->textArea($modelContactsForm,'message', array(
						'class'=>'text requiredField m-bot-20',
						'onblur'=>"if(this.value=='')this.value='{$modelContactsForm->getAttributeLabel('message')}';",
						'onfocus'=>"if(this.value=='{$modelContactsForm->getAttributeLabel('message')}')this.value='';",
						'value'=>$modelContactsForm->getAttributeLabel('message'),
					)); ?>
					<?php echo $form->error($modelContactsForm,'message',array('class' => 'styled-box error')); ?>
					<!--<textarea name="message" id="Mymessage" rows="5" cols="30" class="text requiredField" onblur="if(this.value=='')this.value='Your message...';" onfocus="if(this.value=='Your message...')this.value='';"   >Your message...</textarea>-->
				</fieldset>
				<fieldset class="right m-t-min-1">
					<!--<input name="Mysubmitted" id="Mysubmitted" value="ОТПРАВИТЬ" class="button medium" type="submit" >-->
					<?php echo CHtml::submitButton('ОТПРАВИТЬ',array('onclick'=>'return sendContact();', 'class'=>'button medium')); ?>
				</fieldset>
			<?php $this->endWidget(); ?>
		</div>
		<script type="text/javascript">				 
			function sendContact() {
				var data=jQuery("#contact-form").serialize();			 
			 
			  jQuery.ajax({
				  type: 'POST',
				  url: '<?php echo Yii::app()->createAbsoluteUrl("contacts"); ?>',
				  data:data,
				  dataType: 'json',
					success : function(data) {
			      if(data.status=="success") {
			        jQuery(".contact-form-container").html('<p class="styled-box iconed-box success">Спасибо, ваше сообщение успешно отправлено. Мы постараемся ответить вам в близжайшее время.</p>');
			        jQuery(".contact-form-container .success").fadeIn('slow');
			      } else {
			        jQuery.each(data, function(key, val) {
			          jQuery("#contact-form #"+key+"_em_").text(val);
			          jQuery("#contact-form #"+key+"_em_").show();
			        });
			      }
			    }
			  });
			  
			  return false;			 
			}
		</script>	
		
	</div>


	<!-- SIDEBAR -->
	<div class="four columns  m-bot-25">

		<div class="caption-container-main m-bot-30">
			<div class="caption-text-container">НАШИ <span class="bold">КОНТАКТЫ</span></div>
			<div class="content-container-white caption-bg "></div>
		</div>

		<div class="">
			<ul class="contact-list">
				<li class="contact-loc ">
					г.Харьков, 
					<br>пр. Московский, 94 
					<br>ДК ХЭМЗ 2-й этаж
				</li>
				<li class="contact-phone ">
					<p class="m-top-10">
					(057) 716 93-11
					<br><?php /*(093) 579 43-83*/?>
					</p>
				</li>
				<li class="contact-mail ">
					<p class="m-top-10">
						<a href="mailto:mail@hg.kh.ua">mail@hg.kh.ua</a>
					</p><br>
				</li>
			</ul>				
		</div>		
	</div>

</div>	

<!--GOOLE MAP-->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="js/jquery.gmap.min.js"></script>

<script type="text/javascript">
	jQuery('#googlemaps').gMap({
		maptype: 'ROADMAP',
		scrollwheel: false,
		zoom: 16,
		markers: [
			{
				address: 'Украина, Харьковская область, Харьков, пр. Московский 94', // Your Adress Here
				html: 'ДК ХЭМЗ, Центральный вход,<br> 2 этаж, воскресенье 18-00',
				popup: true,
			}
		],
	});
</script>