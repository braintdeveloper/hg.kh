<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';

$this->layout = '//layouts/clean';
?>

<div class="span5 offset3">
	<?php /*<h1>Login</h1>

	<p>Please fill out the following form with your login credentials:</p>
	*/?>
	
	<div class="form">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
		'htmlOptions'=>array(
			'class'=>'form-vertical'
		)
	)); ?>
	
		<div class="text-center">
		<img src="/images/logoAdmin.png" class="logo" alt="Visine">
	</div>

		<?php //echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldRow($model,'username',array('class'=>'span4','maxlength'=>256)); ?>

		<?php echo $form->passwordFieldRow($model,'password',array('class'=>'span4')); ?>
		
		<span class="help-block">Для доступа к защищенной части сайта, заполните форму необходимыми данными и выполните вход.</span>
		
		<div class="form-actions">
				<span class="pull-left">
					<?php echo $form->checkBoxRow($model,'rememberMe',array('label'=>'Запомнить')); ?>
				</span>

				<span class="pull-right">
					<?php $this->widget('bootstrap.widgets.TbButton', array(
						'buttonType'=>'submit',
						'type'=>'primary',
						'label'=>Yii::t('Phrases', 'Login'),
					)); ?>
				</span>
		</div>		
		
	<?php $this->endWidget(); ?>
	</div><!-- form -->
</div>


<style type="text/css">
/*<![CDATA[*/

		form.form-vertical {
			margin-bottom: 0;
		}

		div.form {
			/* align center */
			position: absolute;

			top: 40%;
			margin-top: -166px;

			left: 50%;
			margin-left: -185px !important;

			/* sizing */
			width: 370px;
			/*height: 400px;*/
			height: auto;
			margin-left: auto;
			margin-right: auto;

			padding: 20px;

			/*styles*/
			-webkit-box-shadow: 1px 1px 30px rgba(50, 50, 50, 0.69);
			-moz-box-shadow:    1px 1px 30px rgba(50, 50, 50, 0.69);
			box-shadow:         1px 1px 30px rgba(50, 50, 50, 0.69);
		}

		div.form-actions {
			padding-left: 20px !important;
		}

		div.controls {
			margin-left: 0 !important;
		}
		
		div.form .error{
			font-size: smaller;
		}
		
	
/*]]>*/
</style>