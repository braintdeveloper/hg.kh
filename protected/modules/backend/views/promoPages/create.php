<?php
$this->breadcrumbs=array(
	'Promo Pages'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List PromoPages','url'=>array('index')),
array('label'=>'Manage PromoPages','url'=>array('admin')),
);
?>

<h1>Create PromoPages</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>