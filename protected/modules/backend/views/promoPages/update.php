<?php
$this->breadcrumbs=array(
	'Promo Pages'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List PromoPages','url'=>array('index')),
	array('label'=>'Create PromoPages','url'=>array('create')),
	array('label'=>'View PromoPages','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PromoPages','url'=>array('admin')),
	);
	?>

	<h1>Update PromoPages <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>