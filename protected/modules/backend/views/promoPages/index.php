<?php
$this->breadcrumbs=array(
	'Promo Pages',
);

$this->menu=array(
array('label'=>'Create PromoPages','url'=>array('create')),
array('label'=>'Manage PromoPages','url'=>array('admin')),
);
?>

<h1>Promo Pages</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
