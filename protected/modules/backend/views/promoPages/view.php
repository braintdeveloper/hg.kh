<?php
$this->breadcrumbs=array(
	'Promo Pages'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List PromoPages','url'=>array('index')),
array('label'=>'Create PromoPages','url'=>array('create')),
array('label'=>'Update PromoPages','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete PromoPages','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage PromoPages','url'=>array('admin')),
);
?>

<h1>View PromoPages #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'page',
		'name_uk',
		'name_ru',
		'description_uk',
		'description_ru',
		//'timeupdate',
		'sort',
),
)); ?>
