<?php
$this->breadcrumbs=array(
	'Promo Texts'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List PromoTexts','url'=>array('index')),
array('label'=>'Create PromoTexts','url'=>array('create')),
array('label'=>'Update PromoTexts','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete PromoTexts','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage PromoTexts','url'=>array('admin')),
);
?>

<h1>View PromoTexts #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name_uk',
		'name_ru',
		'text_uk',
		'text_ru',
		'text2_uk',
		'text2_ru',
		'sort',
		//'timeupdate',
),
)); ?>
