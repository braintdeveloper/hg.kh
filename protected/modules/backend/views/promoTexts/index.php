<?php
$this->breadcrumbs=array(
	'Promo Texts',
);

$this->menu=array(
array('label'=>'Create PromoTexts','url'=>array('create')),
array('label'=>'Manage PromoTexts','url'=>array('admin')),
);
?>

<h1>Promo Texts</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
