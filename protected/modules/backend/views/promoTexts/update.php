<?php
$this->breadcrumbs=array(
	'Promo Texts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List PromoTexts','url'=>array('index')),
	array('label'=>'Create PromoTexts','url'=>array('create')),
	array('label'=>'View PromoTexts','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PromoTexts','url'=>array('admin')),
	);
	?>

	<h1>Update PromoTexts <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>