<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'promo-texts-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data'
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<div class="">
		<span>Size - ZZZxZZZ px</span>
		<?php echo $form->fileField($model,'image',array('class'=>'span5')); ?>		
		<?php
			if($model->image = $model->getPhotoUrl($model->id, $model->timeupdate)){
				echo Chtml::image($model->image, 'image', array('width'=>'100px'));
			}
		?>		
	</div>
	<br>

	<?php 
	foreach (Yii::app()->params['translatedLanguages'] as $suffix => $lang) {
		$nameTab[$lang]['label'] = $lang;
		$nameTab[$lang]['content'] = 
			$form->textFieldRow($model,'name_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textAreaRow($model,'text_'.$suffix,array('rows'=>6, 'cols'=>50, 'class'=>'span8 wysiwyg')).
			$form->textAreaRow($model,'text2_'.$suffix,array('rows'=>6, 'cols'=>50, 'class'=>'span8 wysiwyg'));
			
		if($suffix == Yii::app()->params['defaultLanguage']){
			$nameTab[$lang]['active'] = true;
		}
		?>	
	<?php } ?>
	
	<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'htmlOptions'=>array('class'=>'well well-large'),
    'tabs'=>$nameTab,
	)); ?>	

	<?php if(!$model->isNewRecord){ ?>
	<?php echo $form->textFieldRow($model,'sort',array('class'=>'span5')); ?>
	<?php } ?>

	<?php //echo $form->textFieldRow($model,'timeupdate',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
