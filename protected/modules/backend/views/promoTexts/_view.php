<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_uk')); ?>:</b>
	<?php echo CHtml::encode($data->name_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_ru')); ?>:</b>
	<?php echo CHtml::encode($data->name_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_uk')); ?>:</b>
	<?php echo CHtml::encode($data->text_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_ru')); ?>:</b>
	<?php echo CHtml::encode($data->text_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text2_uk')); ?>:</b>
	<?php echo CHtml::encode($data->text2_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text2_ru')); ?>:</b>
	<?php echo CHtml::encode($data->text2_ru); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sort')); ?>:</b>
	<?php echo CHtml::encode($data->sort); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeupdate')); ?>:</b>
	<?php echo CHtml::encode($data->timeupdate); ?>
	<br />

	*/ ?>

</div>