<?php
$this->breadcrumbs=array(
	'Promo Texts'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List PromoTexts','url'=>array('index')),
array('label'=>'Manage PromoTexts','url'=>array('admin')),
);
?>

<h1>Create PromoTexts</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>