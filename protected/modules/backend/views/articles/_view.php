<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_ru')); ?>:</b>
	<?php echo CHtml::encode($data->name_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anons_ru')); ?>:</b>
	<?php echo CHtml::encode($data->anons_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content_ru')); ?>:</b>
	<?php echo CHtml::encode($data->content_ru); ?>
	<br />

</div>