<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'articles-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data'
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<ul class="clearfix">
		<li class="span4">
			<div class="">
				<span>Главная страница - Слайдер фон | Size - 2500x666 px</span>
				<?php echo $form->fileField($model,'image_sliderBg'); ?>
				<?php
					$model->image_sliderBg = $model->getPhotoNameUrl($model->id.'.sBg.jpg', $model->timeupdate);
					if(@$model->image_sliderBg){
						echo Chtml::image($model->image_sliderBg, 'image_sliderBg', array('width'=>'100px'));
					}
				?>		
			</div>
		</li>
		<li class="span4">
			<div class="">
				<span>Главная страница - Слайдер картинка | Size - 400x400 (от 300 до 400px) подходит</span>
				<?php echo $form->fileField($model,'image_slider'); ?>
				<?php
					$model->image_slider = $model->getPhotoNameUrl($model->id.'.s.jpg', $model->timeupdate);
					if(@$model->image_slider){
						echo Chtml::image($model->image_slider, 'image_slider', array('width'=>'100px'));
					}
				?>		
			</div>
		</li>
		<li class="span4">
			<div class="">
				<span>Основная картинка - Size - 800px по ширине</span>
				<?php echo $form->fileField($model,'image'); ?>
				<?php
					$model->image = $model->getPhotoNameUrl($model->id.'.jpg', $model->timeupdate);
					if(@$model->image){
						echo Chtml::image($model->image, 'image', array('width'=>'100px'));
					}
				?>		
			</div>
		</li>
	</ul>

	<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>256)); ?>	
	
	<?php echo $form->label($model,'timework'); ?>	
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
		    'name'=>'Articles[timework]',
		    'value' => CDates::toEditorDate($model->timework),
		    // additional javascript options for the date picker plugin
		    'options'=>array(
		    	'dateFormat' => 'yy/mm/dd',
		    	'showAnim'=>'slideDown',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
		    ),
		    'htmlOptions'=>array(
		      //'style'=>'height:20px;background-color:green;color:white;',
		    ),
		));
	?>
	<div></div>
	
	
	<?php //echo $form->dropDownListRow($model, 'id_category', CHtml::listData(ArticlesCategory::model()->findAll(),'id','category_ru'),array('class'=>'span5')); ?>	
	
	<?php 
	foreach (Yii::app()->params['translatedLanguages'] as $suffix => $lang) {
		$nameTab[$lang]['label'] = $lang;
		$nameTab[$lang]['content'] = 
			$form->textFieldRow($model,'htitl_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textFieldRow($model,'hdesc_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textFieldRow($model,'hkeyw_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textFieldRow($model,'name_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textFieldRow($model,'name2_'.$suffix,array('class'=>'span8')).
			$form->textAreaRow($model,'anons_'.$suffix,array('rows'=>6, 'cols'=>50, 'class'=>'span8 wysiwyg')).
			$form->textAreaRow($model,'content_'.$suffix,array('rows'=>6, 'cols'=>50, 'class'=>'span8 wysiwyg'));
			
		if($suffix == Yii::app()->params['defaultLanguage']){
			$nameTab[$lang]['active'] = true;
		}
		?>	
	<?php } ?>
	
	<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'htmlOptions'=>array('class'=>'well well-large'),
    'tabs'=>$nameTab,
	)); ?>	
	
	<?php echo $form->dropDownListRow($model, 'id_author', array('0'=>' - без автора') + CHtml::listData(Authors::model()->findAll(), 'id', 'author')); ?>	

	<?php //echo $form->textFieldRow($model,'htitl_uk',array('class'=>'span5','maxlength'=>256)); ?>

	<?php //echo $form->textFieldRow($model,'htitl_ru',array('class'=>'span5','maxlength'=>256)); ?>

	<?php //echo $form->textFieldRow($model,'hdesc_uk',array('class'=>'span5','maxlength'=>256)); ?>

	<?php //echo $form->textFieldRow($model,'hdesc_ru',array('class'=>'span5','maxlength'=>256)); ?>

	<?php //echo $form->textFieldRow($model,'hkeyw_uk',array('class'=>'span5','maxlength'=>256)); ?>

	<?php //echo $form->textFieldRow($model,'hkeyw_ru',array('class'=>'span5','maxlength'=>256)); ?>

	<?php //echo $form->textFieldRow($model,'name_uk',array('class'=>'span5','maxlength'=>256)); ?>

	<?php //echo $form->textFieldRow($model,'name_ru',array('class'=>'span5','maxlength'=>256)); ?>

	<?php //echo $form->textAreaRow($model,'anons_uk',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textAreaRow($model,'anons_ru',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textAreaRow($model,'content_uk',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textAreaRow($model,'content_ru',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
	
	<?php echo $form->dropDownListRow($model, 'status', $this->statusArr); ?>	
	<?php //echo $form->dropDownListRow($model, 'onslider', $this->statusArr); ?>	

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>



<?php if(!$model->isNewRecord){ ?>

	<br>
	<br>
	<h3>List photos:</h3>
	<form action="<?=Yii::app()->request->baseUrl.'/backend/photos/create'?>" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="Photos[redirect]" value="<?=Yii::app()->request->url;?>">
	<input type="hidden" name="Photos[id_parent]" value="<?=$model->id?>">
	<input type="hidden" name="Photos[type]" value="article">
	<p>Upload photos: <input name="filesToUpload[]" id="filesToUpload" type="file" multiple="" onChange="makeFileList();"/></p>
	<script>
	function makeFileList() {
			var input = document.getElementById("filesToUpload");
			var ul = document.getElementById("fileList");
			while (ul.hasChildNodes()) {
				ul.removeChild(ul.firstChild);
			}
			for (var i = 0; i < input.files.length; i++) {
				var li = document.createElement("li");
				li.innerHTML = input.files[i].name;
				ul.appendChild(li);
			}
			if(!ul.hasChildNodes()) {
				var li = document.createElement("li");
				li.innerHTML = 'No Files Selected';
				ul.appendChild(li);
			}
		}
	</script>
	<div class="notification success png_bg">
		<div><b>Multi Upload photos was allowed</b></div>
		<span>Size - 960x640px</span>
	</div>
	<ul id="fileList"></ul>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
	    'buttonType'=>'submit',
	    'label'=>'Upload images',
	    'block'=>false,
	)); ?>
	</form>
		
	<?php 
	$photosModel = new Photos('search');
	$photosModel->unsetAttributes();  // clear any default values
	if(isset($_GET['Photos']))
			$photosModel->attributes=$_GET['Photos'];

	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'photos-grid',
		'dataProvider'=>$photosModel->search($model->id, 'article'),
		'filter'=>$photosModel,
		//'ajaxUrl'=>Yii::app()->createUrl('/backend/articles/photos'),
		'columns'=>array(
			//'id',
			'sort'=>array(
				'name'=>'sort',
				'htmlOptions'=>array('width'=>"80px"),
			),
			'image'=>array(
		    'name'=>'image',
		    'type'=>'html',
		    'value'=>'CHtml::tag("img", array(
		        "title"=>$data->name,
		        "src"=>Photos::model()->getPhotoUrl($data->id),
		        "style"=>"height:40px",
		    ))',
		    'htmlOptions'=>array('width'=>"80px",'style'=>'text-align:center;'),
			),		
			//'name',
			array(
	        'name' => 'name',
	        'header' => 'Name',
	        'class' => 'bootstrap.widgets.TbEditableColumn',
	        //'headerHtmlOptions' => array('style' => 'width:80px'),
	        'editable' => array(
	            'type' => 'text',
	            //'url' => 'Yii::app()->controller->createUrl("photos/updateAjax",array("id"=>$data->id))',
	            'url' => '/backend/photos/updateAjax/',
	        )
	    ),			
			'status'=>array(
		    'name'=>'status',
		    'filter'=>$this->statusArr,
		    'type'=>'html',
		    'value'=>'CHtml::tag("a", array(
		      "class"=>"updateStatus",
		      "href"=>Yii::app()->controller->createUrl("photos/setStatus",array("id"=>$data->id,"value"=>(1==$data->status)?"0":"1")),
		      ),
	    		CHtml::tag("img", array(
		        "title"=>UtilityHtml::getImagetitle($data->status),
		        "src"=>UtilityHtml::getStatusImage($data->status),
		      ))
		    )',
		    'htmlOptions'=>array('width'=>"80px",'style'=>'text-align:center;'),
			),
			'field'=>array
			(
				'htmlOptions'=>array('width'=>'130px','style'=>'text-align:center;'),
			  'header'=>"Функции",
			  'class'=>'CButtonColumn',
			  'template'=>'{up} {down} {updateStatus} {delete}',
			  'buttons'=>array(
					'updateStatus' => array(
					  'label'=>'',
					  //'imageUrl'=>'',
					  'click'=>"function(){
					    $.fn.yiiGridView.update('photos-grid', {
					        type:'POST',
					        url:$(this).attr('href'),
					        success:function(data) {
			        			$('#AjFlash').html(data).stop(true, true).fadeIn().animate({opacity: 1.0}, 2000).fadeOut('slow');
					         $.fn.yiiGridView.update('photos-grid');
					        }
					    })
					    return false;
					  }",
					  'url'=>'Yii::app()->controller->createUrl("photos/setStatus",array("id"=>$data->id,"value"=>(1==$data->status)?"0":"1"))',
					),
					'up' => array(
					  'label'=>'<i class="icon-arrow-down"></i>',
					  //'imageUrl'=> Yii::app()->request->baseUrl.'/images/admin/16x16/download.png',
					  'click'=>"function(){
					    $.fn.yiiGridView.update('photos-grid', {
					        type:'POST',
					        url:$(this).attr('href'),
					        success:function(data) {
			        			$('#AjFlash').html(data).stop(true, true).fadeIn().animate({opacity: 1.0}, 2000).fadeOut('slow');
					         $.fn.yiiGridView.update('photos-grid');
					        }
					    })
					    return false;
					  }",
					  'url'=>'Yii::app()->controller->createUrl("photos/moveRow",array("id"=>$data->id,"value"=>"1","type"=>"article","id_parent"=>"$data->id_parent"))',
					),
					'down' => array(
					  'label'=>'<i class="icon-arrow-up"></i>',
					  //'imageUrl'=> Yii::app()->request->baseUrl.'/images/admin/16x16/upload.png',
					  'click'=>"function(){
					    $.fn.yiiGridView.update('photos-grid', {
					        type:'POST',
					        url:$(this).attr('href'),
					        success:function(data) {
			        			$('#AjFlash').html(data).stop(true, true).fadeIn().animate({opacity: 1.0}, 2000).fadeOut('slow');
					         $.fn.yiiGridView.update('photos-grid');
					        }
					    })
					    return false;
					  }",
					  'url'=>'Yii::app()->controller->createUrl("photos/moveRow",array("id"=>$data->id,"value"=>"-1","type"=>"article","id_parent"=>"$data->id_parent"))',
					),
					'delete' => array(
						//'label'=>'<i class="icon-trash"></i>',
						'url'=>'Yii::app()->controller->createUrl("photos/delete",array("id"=>$data->id))',
						'htmlOptions'=>array('class'=>'icon-trash'),
					)
				)
	    ),
		),
	)); ?>

<?php } else { ?>
	
	<h3>Загрузка списка фотографий доступна только при созданной записи.</h3>
	
<?php } ?>