<?php
	$this->breadcrumbs=array(
		'Articles'=>array('index'),
		'Manage',
	);

	$this->menu=array(
		array('label'=>'List Articles','url'=>array('index')),
		array('label'=>'Create Articles','url'=>array('create')),
	);

	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
		});
		$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('articles-grid', {
		data: $(this).serialize()
		});
		return false;
		});
	");
?>

<h1>Новости</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'articles-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'name_ru',
		'alias',		
		'timework'=>array(
			'name'=>'timework',
			'value'=>'CDates::toEditorDate($data->timework)',
		),
		'views',		
		'status'=>array(
	    'name'=>'status',
	    'filter'=>$this->statusArr,
	    'type'=>'html',
	    'value'=>'CHtml::tag("a", array(
	      "class"=>"updateStatus",
	      "href"=>Yii::app()->controller->createUrl("setStatus",array("id"=>$data->id,"value"=>(1==$data->status)?"0":"1")),
	      ),
	    	CHtml::tag("img", array(
	        "title"=>UtilityHtml::getImagetitle($data->status),
	        "src"=>UtilityHtml::getStatusImage($data->status),
	      ))
	    )',
	    'htmlOptions'=>array('width'=>"80px",'style'=>'text-align:center;'),
		),
		'field'=>array
		(
			//'htmlOptions'=>array('width'=>'130px','style'=>'text-align:center;'),
		  //'header'=>"Функции",
		  'class'=>'CButtonColumn',
		  'template'=>'{up} {down} {updateStatus} {updateOnSliderStatus}',
		  'buttons'=>array(
				'updateStatus' => array(
				  'label'=>'',
				  //'imageUrl'=>'',
				  'click'=>"function(){
				    $.fn.yiiGridView.update('articles-grid', {
				        type:'POST',
				        url:$(this).attr('href'),
				        success:function(data) {
			        		$('#AjFlash').html(data).stop(true, true).fadeIn().animate({opacity: 1.0}, 2000).fadeOut('slow');
				         	$.fn.yiiGridView.update('articles-grid');
				        }
				    })
				    return false;
				  }",
				  'url'=>'Yii::app()->controller->createUrl("setStatus",array("id"=>$data->id,"value"=>(1==$data->status)?"0":"1"))',
				),
				'updateOnSliderStatus' => array(
				  'label'=>'',
				  //'imageUrl'=>'',
				  'click'=>"function(){
				    $.fn.yiiGridView.update('articles-grid', {
				        type:'POST',
				        url:$(this).attr('href'),
				        success:function(data) {
			        		$('#AjFlash').html(data).stop(true, true).fadeIn().animate({opacity: 1.0}, 2000).fadeOut('slow');
				         	$.fn.yiiGridView.update('articles-grid');
				        }
				    })
				    return false;
				  }",
				  'url'=>'Yii::app()->controller->createUrl("setOnSliderStatus",array("id"=>$data->id,"value"=>(1==$data->status)?"0":"1"))',
				),
				'up' => array(
				  'label'=>'<i class="icon-arrow-down"></i>',
				  'class'=> 'icon-pencil',
				  //'imageUrl'=> Yii::app()->request->baseUrl.'/images/admin/16x16/download.png',
				  'click'=>"function(){
				    $.fn.yiiGridView.update('articles-grid', {
				        type:'POST',
				        url:$(this).attr('href'),
				        success:function(data) {
			        		$('#AjFlash').html(data).stop(true, true).fadeIn().animate({opacity: 1.0}, 2000).fadeOut('slow');
				         $.fn.yiiGridView.update('articles-grid');
				        }
				    })
				    return false;
				  }",
				  'url'=>'Yii::app()->controller->createUrl("moveRow",array("id"=>$data->id,"value"=>"1"))',
				),
				'down' => array(
				  'label'=>'<i class="icon-arrow-up"></i>',
				  //'imageUrl'=> Yii::app()->request->baseUrl.'/images/admin/16x16/upload.png',
				  'click'=>"function(){
				    $.fn.yiiGridView.update('articles-grid', {
				        type:'POST',
				        url:$(this).attr('href'),
				        success:function(data) {
			        		$('#AjFlash').html(data).stop(true, true).fadeIn().animate({opacity: 1.0}, 2000).fadeOut('slow');
				         $.fn.yiiGridView.update('articles-grid');
				        }
				    })
				    return false;
				  }",
				  'url'=>'Yii::app()->controller->createUrl("moveRow",array("id"=>$data->id,"value"=>"-1"))',
				),				
			)
    ),		
		/*'htitl_uk',
		'htitl_ru',
		'hdesc_uk',		
		'hdesc_ru',
		'hkeyw_uk',
		'hkeyw_ru',
		'name_uk',
		'name_ru',
		'anons_uk',
		'anons_ru',
		'content_uk',
		'content_ru',
		'timecreate',
		'timeupdate',
		'status',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
	)); ?>
