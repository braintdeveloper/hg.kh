<?php
$this->breadcrumbs=array(
	'Video Categories'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List VideoCategory','url'=>array('index')),
array('label'=>'Create VideoCategory','url'=>array('create')),
array('label'=>'Update VideoCategory','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete VideoCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage VideoCategory','url'=>array('admin')),
);
?>

<h1>View VideoCategory #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'alias',
		'category_ru',
),
)); ?>
