<?php
$this->breadcrumbs=array(
	'Video Categories',
);

$this->menu=array(
array('label'=>'Create VideoCategory','url'=>array('create')),
array('label'=>'Manage VideoCategory','url'=>array('admin')),
);
?>

<h1>Video Categories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
