<?php
$this->breadcrumbs=array(
	'Video Categories'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List VideoCategory','url'=>array('index')),
array('label'=>'Manage VideoCategory','url'=>array('admin')),
);
?>

<h1>Create VideoCategory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>