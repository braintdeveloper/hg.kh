<?php
	$this->breadcrumbs=array(
		'Questions'=>array('index'),
		'Manage',
	);

	$this->menu=array(
		array('label'=>'List Questions','url'=>array('index')),
		array('label'=>'Create Questions','url'=>array('create')),
	);

	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
		});
		$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('questions-grid', {
		data: $(this).serialize()
		});
		return false;
		});
	");
?>

<h1>Manage Questions</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'questions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'idcategory'=>array(
			'name'=>'idcategory',
			'filter'=>CHtml::listData(QuestionsCategory::model()->findAll(), 'id', 'category_uk'),
			'value'=> '$data->questionsCategory0->category_uk',			
		),
		'alias',
		'name_uk',
		'name_ru',
		'status'=>array(
			'name'=>'status',
			'filter'=>$this->statusArr,
			'type'=>'html',
			'value'=>'CHtml::tag("a", array(
				"class"=>"updateStatus",
				"href"=>Yii::app()->controller->createUrl("setStatus",array("id"=>$data->id,"value"=>(1==$data->status)?"0":"1")),
				),
				CHtml::tag("img", array(
					"title"=>UtilityHtml::getImagetitle($data->status),
					"src"=>UtilityHtml::getStatusImage($data->status),
				))
			)',
			'htmlOptions'=>array('width'=>"80px",'style'=>'text-align:center;'),
		),
		'field'=>array(
		  'class'=>'CButtonColumn',
		  'template'=>'{updateStatus}',
		  'buttons'=>array(
				'updateStatus' => array(
				  'label'=>'',
				  'click'=>"function(){
				    $.fn.yiiGridView.update('questions-grid', {
				        type:'POST',
				        url:$(this).attr('href'),
				        success:function(data) {
			        		$('#AjFlash').html(data).stop(true, true).fadeIn().animate({opacity: 1.0}, 2000).fadeOut('slow');
				         	$.fn.yiiGridView.update('questions-grid');
				        }
				    })
				    return false;
				  }",
				  'url'=>'Yii::app()->controller->createUrl("setStatus",array("id"=>$data->id,"value"=>(1==$data->status)?"0":"1"))',
				),
			)
		),
		/*
		'hdesc_ru',
		'hkeyw_uk',
		'hkeyw_ru',
		'name_uk',
		'name_ru',
		'content_uk',
		'content_ru',
		'status',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
	)); ?>
