<?php
$this->breadcrumbs=array(
	'Textpages'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Textpages','url'=>array('index')),
array('label'=>'Create Textpages','url'=>array('create')),
array('label'=>'Update Textpages','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Textpages','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Textpages','url'=>array('admin')),
);
?>

<h1>View Textpages #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idcategory',
		'alias',
		'htitl_uk',
		'htitl_ru',
		'hdesc_uk',
		'hdesc_ru',
		'hkeyw_uk',
		'hkeyw_ru',
		'name_uk',
		'name_ru',
		'content_uk',
		'content_ru',
		'status',
),
)); ?>
