<?php
$this->breadcrumbs=array(
	'Textpages'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Textpages','url'=>array('index')),
array('label'=>'Manage Textpages','url'=>array('admin')),
);
?>

<h1>Create Textpages</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>