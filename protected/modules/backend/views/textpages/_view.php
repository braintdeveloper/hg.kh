<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('htitl_uk')); ?>:</b>
	<?php echo CHtml::encode($data->htitl_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('htitl_ru')); ?>:</b>
	<?php echo CHtml::encode($data->htitl_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdesc_uk')); ?>:</b>
	<?php echo CHtml::encode($data->hdesc_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdesc_ru')); ?>:</b>
	<?php echo CHtml::encode($data->hdesc_ru); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('hkeyw_uk')); ?>:</b>
	<?php echo CHtml::encode($data->hkeyw_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hkeyw_ru')); ?>:</b>
	<?php echo CHtml::encode($data->hkeyw_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_uk')); ?>:</b>
	<?php echo CHtml::encode($data->name_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_ru')); ?>:</b>
	<?php echo CHtml::encode($data->name_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content_uk')); ?>:</b>
	<?php echo CHtml::encode($data->content_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content_ru')); ?>:</b>
	<?php echo CHtml::encode($data->content_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>