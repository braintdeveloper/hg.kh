<?php
$this->breadcrumbs=array(
	'Textpages'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Textpages','url'=>array('index')),
	array('label'=>'Create Textpages','url'=>array('create')),
	array('label'=>'View Textpages','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Textpages','url'=>array('admin')),
	);
	?>

	<h1>Update Textpages <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>