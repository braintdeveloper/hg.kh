<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'textpages-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data'
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<div class="">
		<span>Size - ZZZxZZZ px</span>
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo CHTMl::activeFileField($model, 'image'); ?>		
		<?php echo $form->error($model,'image'); ?>		
		<?php
			$model->image = $model->getPhotoUrl($model->id, $model->timeupdate);
			if(@$model->image){
				echo Chtml::image($model->image, 'image', array('width'=>'100px'));
			}
		?>		
	</div>
	<br>

	<?php if($model->isNewRecord){ ?>
	<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>256)); ?>	
	<?php } else { ?>
	<?php echo $form->hiddenField($model,'alias',array('class'=>'span5','maxlength'=>256)); ?>	
	<?php } ?>
	
	<?php 
	foreach (Yii::app()->params['translatedLanguages'] as $suffix => $lang) {
		$nameTab[$lang]['label'] = $lang;
		$nameTab[$lang]['content'] = 
			$form->textFieldRow($model,'htitl_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textFieldRow($model,'hdesc_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textFieldRow($model,'hkeyw_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textFieldRow($model,'name_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textAreaRow($model,'content_'.$suffix,array('rows'=>6, 'cols'=>50, 'class'=>'span8 wysiwyg'));
			
		if($suffix == Yii::app()->params['defaultLanguage']){
			$nameTab[$lang]['active'] = true;
		}
		?>	
	<?php } ?>
	
	<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'htmlOptions'=>array('class'=>'well well-large'),
    'tabs'=>$nameTab,
	)); ?>
	
	<?php echo $form->dropDownListRow($model, 'status', $this->statusArr); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
