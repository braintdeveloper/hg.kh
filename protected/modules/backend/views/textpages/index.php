<?php
$this->breadcrumbs=array(
	'Textpages',
);

$this->menu=array(
array('label'=>'Create Textpages','url'=>array('create')),
array('label'=>'Manage Textpages','url'=>array('admin')),
);
?>

<h1>Textpages</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
