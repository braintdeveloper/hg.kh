<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'slider-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data'
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<ul class="clearfix">
		<li class="span4">
			<div class="">
				<span>Big image Uk | Size - 965 x 353 px</span>
				<?php echo $form->fileField($model,'image_uk'); ?>
				<?php
					$model->image_uk = $model->getPhotoNameUrl($model->id.'.uk.jpg', $model->timeupdate);
					if(@$model->image_uk){
						echo Chtml::image($model->image_uk, 'image_uk', array('width'=>'100px'));
					}
				?>		
			</div>
		</li>
		<li class="span4">
			<div class="">
				<span>Big image Ru | Size - 965 x 353 px</span>
				<?php echo $form->fileField($model,'image_ru'); ?>
				<?php
					$model->image_ru = $model->getPhotoNameUrl($model->id.'.ru.jpg', $model->timeupdate);
					if(@$model->image_ru){
						echo Chtml::image($model->image_ru, 'image_ru', array('width'=>'100px'));
					}
				?>		
			</div>
		</li>
	</ul>
	

	<?php 
	foreach (Yii::app()->params['translatedLanguages'] as $suffix => $lang) {
		$nameTab[$lang]['label'] = $lang;
		$nameTab[$lang]['content'] = 
			$form->textFieldRow($model,'name_'.$suffix,array('class'=>'span5','maxlength'=>256)).
			$form->textAreaRow($model,'content_'.$suffix,array('rows'=>6, 'cols'=>50, 'class'=>'span8 wysiwyg'));
			
		if($suffix == Yii::app()->params['defaultLanguage']){
			$nameTab[$lang]['active'] = true;
		}
		?>	
	<?php } ?>
	
	<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'htmlOptions'=>array('class'=>'well well-large'),
    'tabs'=>$nameTab,
	)); ?>		
	
	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->dropDownListRow($model,'status', $this->statusArr, array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'timecreate',array('class'=>'span5','maxlength'=>10)); ?>

	<?php //echo $form->textFieldRow($model,'timeupdate',array('class'=>'span5','maxlength'=>10)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
