<?php
$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Products','url'=>array('index')),
array('label'=>'Create Products','url'=>array('create')),
array('label'=>'Update Products','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Products','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Products','url'=>array('admin')),
);
?>

<h1>View Products #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		//'idquestion',
		//'idbanner1',
		//'idbanner2',
		'alias',
		'price',
		'htitl_uk',
		'htitl_ru',
		'hdesc_uk',
		'hdesc_ru',
		'hkeyw_uk',
		'hkeyw_ru',
		'name_uk',
		'name_ru',
		'anons_uk',
		'anons_ru',
		'anons2_uk',
		'anons2_ru',
		'content_uk',
		'content_ru',
		'help1_uk',
		'help1_ru',
		//'timecreate',
		//'timeupdate',
		'status',
		'sort',
),
)); ?>
