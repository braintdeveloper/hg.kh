<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5','maxlength'=>11)); ?>

		<?php echo $form->textFieldRow($model,'idquestion',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idbanner1',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idbanner2',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>256)); ?>

		<?php echo $form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'name_uk',array('class'=>'span5','maxlength'=>256)); ?>

		<?php echo $form->textFieldRow($model,'name_ru',array('class'=>'span5','maxlength'=>256)); ?>

		<?php echo $form->textFieldRow($model,'sort',array('class'=>'span5','maxlength'=>10)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
