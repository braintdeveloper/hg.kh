<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_uk')); ?>:</b>
	<?php echo CHtml::encode($data->name_uk); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('htitl_ru')); ?>:</b>
	<?php echo CHtml::encode($data->htitl_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdesc_uk')); ?>:</b>
	<?php echo CHtml::encode($data->hdesc_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdesc_ru')); ?>:</b>
	<?php echo CHtml::encode($data->hdesc_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hkeyw_uk')); ?>:</b>
	<?php echo CHtml::encode($data->hkeyw_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hkeyw_ru')); ?>:</b>
	<?php echo CHtml::encode($data->hkeyw_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_uk')); ?>:</b>
	<?php echo CHtml::encode($data->name_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_ru')); ?>:</b>
	<?php echo CHtml::encode($data->name_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anons_uk')); ?>:</b>
	<?php echo CHtml::encode($data->anons_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anons_ru')); ?>:</b>
	<?php echo CHtml::encode($data->anons_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anons2_uk')); ?>:</b>
	<?php echo CHtml::encode($data->anons2_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anons2_ru')); ?>:</b>
	<?php echo CHtml::encode($data->anons2_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content_uk')); ?>:</b>
	<?php echo CHtml::encode($data->content_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content_ru')); ?>:</b>
	<?php echo CHtml::encode($data->content_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('help1_uk')); ?>:</b>
	<?php echo CHtml::encode($data->help1_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('help1_ru')); ?>:</b>
	<?php echo CHtml::encode($data->help1_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('help2_uk')); ?>:</b>
	<?php echo CHtml::encode($data->help2_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('help2_ru')); ?>:</b>
	<?php echo CHtml::encode($data->help2_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('help3_uk')); ?>:</b>
	<?php echo CHtml::encode($data->help3_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('help3_ru')); ?>:</b>
	<?php echo CHtml::encode($data->help3_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timecreate')); ?>:</b>
	<?php echo CHtml::encode($data->timecreate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeupdate')); ?>:</b>
	<?php echo CHtml::encode($data->timeupdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sort')); ?>:</b>
	<?php echo CHtml::encode($data->sort); ?>
	<br />

	*/ ?>

</div>