<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'products-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data'
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<ul class="clearfix">
		<li class="span2">
			<div class="">
				<span>На главную страницу - Size - 102x187 px</span>
				<?php echo $form->fileField($model,'image'); ?>
				<?php
					$model->image = $model->getPhotoUrl($model->id, $model->timeupdate);
					if(@$model->image){
						echo Chtml::image($model->image, 'image', array('width'=>'100px'));
					}
				?>		
			</div>
		</li>
		<li class="span2">
			<div class="">
				<span>На страницу продуктов - Size - 146x259 px</span>
				<?php echo $form->fileField($model,'image2'); ?>
				<?php
					$model->image2 = $model->getPhotoNameUrl($model->id.'-2.jpg', $model->timeupdate);
					if(@$model->image2){
						echo Chtml::image($model->image2, 'image', array('width'=>'100px'));
					}
				?>		
			</div>		
		</li>
		<li class="span2">
			<div class="">
				<span>На страницу продукта - Size - 146x259 px</span>
				<?php echo $form->fileField($model,'image3'); ?>
				<?php
					$model->image3 = $model->getPhotoNameUrl($model->id.'-3.jpg', $model->timeupdate);
					if(@$model->image3){
						echo Chtml::image($model->image3, 'image', array('width'=>'100px'));
					}
				?>		
			</div>		
		</li>
		<?php /*<li class="span4">
			<div class="">
				<span>PDF</span>
				<?php echo $form->fileField($model,'pdf'); ?>		
				<p>
				<?php
					$model->pdf = $model->getPhotoNameUrl($model->id.'.pdf');
					if(@$model->pdf){
						echo Chtml::link('PDF is Loaded', $model->pdf);
					}
				?>	
				</p>	
			</div>
		</li>*/?>
	</ul>
	
	<?php echo $form->checkBoxRow($model, 'sample'); ?>
	
	<?php //echo $form->dropDownListRow($model, 'idquestion', CHtml::listData(QuestionsCategory::model()->findAll(), 'id', 'category_uk')); ?>

	<?php //echo $form->dropDownListRow($model, 'idbanner1', ( array('0'=>'-') + CHtml::listData(Banners::model()->findAll(), 'id', 'name_uk')) ); ?>
	
	<?php //echo $form->dropDownListRow($model, 'idbanner2',  ( array('0'=>'-') + CHtml::listData(Banners::model()->findAll(), 'id', 'name_uk')) ); ?>
	
	<?php //echo $form->dropDownListRow($model, 'idbanner3',  ( array('0'=>'-') + CHtml::listData(Banners::model()->findAll(), 'id', 'name_uk')) ); ?>
	
	<?php if((int)$model->id) { ?>
		<?php $model->productsused=CHtml::listData(ProductsToUsed::model()->findAll('idproducts='.$model->id), 'idused', 'idused');?>
	<?php } ?>
	<?php echo $form->dropDownListRow(
		$model, 
		'productsused',  
		CHtml::listData(ProductsUsed::model()->findAll(), 'id', 'name_uk'),
		array('multiple'=>true, 'size'=>4));
	?>
	
	
	<?php echo $form->textFieldRow($model,'alias',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'price',array('class'=>'span5','maxlength'=>10)); ?>
	
	<?php echo $form->textFieldRow($model,'cents',array('class'=>'span5','maxlength'=>2)); ?>
	
	<?php 
	foreach (Yii::app()->params['translatedLanguages'] as $suffix => $lang) {
		$nameTab[$lang]['label'] = $lang;
		$nameTab[$lang]['content'] = 
			$form->textFieldRow($model,'htitl_'.$suffix,array('class'=>'span5','maxlength'=>255)).
			$form->textFieldRow($model,'hdesc_'.$suffix,array('class'=>'span5','maxlength'=>255)).
			$form->textFieldRow($model,'hkeyw_'.$suffix,array('class'=>'span5','maxlength'=>255)).'<br><br>'.
			$form->textFieldRow($model,'name_'.$suffix,array('class'=>'span5','maxlength'=>255)).
			$form->textAreaRow($model,'content_'.$suffix,array('rows'=>6, 'cols'=>50, 'class'=>'span8 wysiwyg')).
			$form->textAreaRow($model,'text1_'.$suffix,array('rows'=>3, 'cols'=>50, 'class'=>'span8')).'<br><br>'.
			$form->textFieldRow($model,'mainname_'.$suffix,array('class'=>'span5','maxlength'=>255)).'<br><br>'.
			$form->textFieldRow($model,'anonsname_'.$suffix,array('class'=>'span5','maxlength'=>255)).
			$form->textAreaRow($model,'anonstext_'.$suffix,array('rows'=>3, 'cols'=>50, 'class'=>'span8'))
			;
			
		if($suffix == Yii::app()->params['defaultLanguage']){
			$nameTab[$lang]['active'] = true;
		}
		?>	
	<?php } ?>
	
	<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'htmlOptions'=>array('class'=>'well well-large'),
    'tabs'=>$nameTab,
	)); ?>	

	<?php echo $form->dropDownListRow($model, 'status', $this->statusArr); ?>

	<?php echo $form->textFieldRow($model,'sort',array('class'=>'span2','maxlength'=>10)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
