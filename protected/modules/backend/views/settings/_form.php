<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'settings-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<p><strong><?php echo "Name: ".$model->name; ?></strong></p>
	<?php if($model->isNewRecord){ ?>
		<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>256)); ?>
	<?php } else { ?>
		<?php echo $form->hiddenField($model,'name',array('class'=>'span5','maxlength'=>256)); ?>
	<?php } ?>

	<?php echo $form->textAreaRow($model,'value',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
