<?php
$this->breadcrumbs=array(
	'Products Useds'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List ProductsUsed','url'=>array('index')),
	array('label'=>'Create ProductsUsed','url'=>array('create')),
	array('label'=>'View ProductsUsed','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ProductsUsed','url'=>array('admin')),
	);
	?>

	<h1>Update ProductsUsed <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>