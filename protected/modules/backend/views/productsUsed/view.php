<?php
$this->breadcrumbs=array(
	'Products Useds'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ProductsUsed','url'=>array('index')),
array('label'=>'Create ProductsUsed','url'=>array('create')),
array('label'=>'Update ProductsUsed','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ProductsUsed','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ProductsUsed','url'=>array('admin')),
);
?>

<h1>View ProductsUsed #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name_uk',
		'name_ru',
),
)); ?>
