<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'products-used-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data'
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<div class="">
		<span>Size - ZZZxZZZ px</span>
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo CHTMl::activeFileField($model, 'image'); ?>		
		<?php echo $form->error($model,'image'); ?>		
		<?php
			$model->image = $model->getPhotoUrl($model->id, $model->timeupdate);
			if(@$model->image){
				echo Chtml::image($model->image, 'image', array('width'=>'100px'));
			}
		?>		
	</div>
	<br>

	<?php echo $form->textFieldRow($model,'name_uk',array('class'=>'span5','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'name_ru',array('class'=>'span5','maxlength'=>256)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
