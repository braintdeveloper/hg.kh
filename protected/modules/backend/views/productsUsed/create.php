<?php
$this->breadcrumbs=array(
	'Products Useds'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ProductsUsed','url'=>array('index')),
array('label'=>'Manage ProductsUsed','url'=>array('admin')),
);
?>

<h1>Create ProductsUsed</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>