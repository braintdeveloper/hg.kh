<?php
$this->breadcrumbs=array(
	'Articles Categories',
);

$this->menu=array(
array('label'=>'Create ArticlesCategory','url'=>array('create')),
array('label'=>'Manage ArticlesCategory','url'=>array('admin')),
);
?>

<h1>Articles Categories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
