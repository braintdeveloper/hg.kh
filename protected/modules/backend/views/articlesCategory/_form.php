<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'articles-category-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->textFieldRow($model,'category_uk',array('class'=>'span5','maxlength'=>256)); ?>

	<?php //echo $form->textFieldRow($model,'category_ru',array('class'=>'span5','maxlength'=>256)); ?>
	
	<?php 
	foreach (Yii::app()->params['translatedLanguages'] as $suffix => $lang) {
		$nameTab[$lang]['label'] = $lang;
		$nameTab[$lang]['content'] = 
			$form->labelEx($model, 'category').
			$form->textField($model, 'category'.'_'.$suffix, array('size'=>60, 'maxlength'=>255)).
			$form->error($model, 'category'.$suffix);
		if($suffix == Yii::app()->params['defaultLanguage']){
			$nameTab[$lang]['active'] = true;
		}
		?>	
	<?php } ?>
	
	<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'tabs'=>$nameTab,
	)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
