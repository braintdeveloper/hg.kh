<?php
$this->breadcrumbs=array(
	'Articles Categories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List ArticlesCategory','url'=>array('index')),
	array('label'=>'Create ArticlesCategory','url'=>array('create')),
	array('label'=>'View ArticlesCategory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ArticlesCategory','url'=>array('admin')),
	);
	?>

	<h1>Update ArticlesCategory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>