<?php
$this->breadcrumbs=array(
	'Articles Categories'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ArticlesCategory','url'=>array('index')),
array('label'=>'Create ArticlesCategory','url'=>array('create')),
array('label'=>'Update ArticlesCategory','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ArticlesCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ArticlesCategory','url'=>array('admin')),
);
?>

<h1>View ArticlesCategory #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'category_uk',
		'category_ru',
),
)); ?>
