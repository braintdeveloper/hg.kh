<?php
$this->breadcrumbs=array(
	'Articles Categories'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ArticlesCategory','url'=>array('index')),
array('label'=>'Manage ArticlesCategory','url'=>array('admin')),
);
?>

<h1>Create ArticlesCategory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>