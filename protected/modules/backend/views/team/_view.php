<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('icons')); ?>:</b>
	<?php echo CHtml::encode($data->icons); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timecreate')); ?>:</b>
	<?php echo CHtml::encode($data->timecreate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeupdate')); ?>:</b>
	<?php echo CHtml::encode($data->timeupdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sort')); ?>:</b>
	<?php echo CHtml::encode($data->sort); ?>
	<br />


</div>