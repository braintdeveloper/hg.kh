<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'team-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data'
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<ul class="clearfix">
		<li class="span4">
			<div class="">
				<span>Image | Size - 600x400 px</span>
				<?php echo $form->fileField($model,'image'); ?>
				<?php
					$model->image = $model->getPhotoNameUrl($model->id.'.jpg', $model->timeupdate);
					if(@$model->image){
						echo Chtml::image($model->image, 'image', array('width'=>'100px'));
					}
				?>		
			</div>
		</li>
	</ul>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'description',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textAreaRow($model,'icons',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textFieldRow($model,'timecreate',array('class'=>'span5','maxlength'=>11)); ?>

	<?php //echo $form->textFieldRow($model,'timeupdate',array('class'=>'span5','maxlength'=>11)); ?>

	<?php //echo $form->textFieldRow($model,'sort',array('class'=>'span5','maxlength'=>11)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
