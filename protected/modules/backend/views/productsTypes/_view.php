<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_uk')); ?>:</b>
	<?php echo CHtml::encode($data->name_uk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_ru')); ?>:</b>
	<?php echo CHtml::encode($data->name_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timecreate')); ?>:</b>
	<?php echo CHtml::encode($data->timecreate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeupdate')); ?>:</b>
	<?php echo CHtml::encode($data->timeupdate); ?>
	<br />


</div>