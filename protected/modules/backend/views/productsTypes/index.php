<?php
$this->breadcrumbs=array(
	'Products Types',
);

$this->menu=array(
array('label'=>'Create ProductsTypes','url'=>array('create')),
array('label'=>'Manage ProductsTypes','url'=>array('admin')),
);
?>

<h1>Products Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
