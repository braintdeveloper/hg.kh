<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'products-types-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data'
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<ul class="clearfix">
		<li class="span3">
			<div class="">
				<span>На главную страницу - Size - 116x216 px</span>
				<?php echo $form->fileField($model,'image'); ?>
				<?php
					$model->image = $model->getPhotoUrl($model->id, $model->timeupdate);
					if(@$model->image){
						echo Chtml::image($model->image, 'image', array('width'=>'100px'));
					}
				?>		
			</div>
		</li>
	</ul>

	<?php echo $form->textFieldRow($model,'name_uk',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'name_ru',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php echo $form->textFieldRow($model,'price',array('class'=>'span2','maxlength'=>3)); ?>
	
	<?php echo $form->textFieldRow($model,'cents',array('class'=>'span2','maxlength'=>2)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
