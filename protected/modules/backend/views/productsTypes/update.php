<?php
$this->breadcrumbs=array(
	'Products Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List ProductsTypes','url'=>array('index')),
	array('label'=>'Create ProductsTypes','url'=>array('create')),
	array('label'=>'View ProductsTypes','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ProductsTypes','url'=>array('admin')),
	);
	?>

	<h1>Update ProductsTypes <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>