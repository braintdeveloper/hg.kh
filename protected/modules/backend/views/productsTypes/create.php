<?php
$this->breadcrumbs=array(
	'Products Types'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ProductsTypes','url'=>array('index')),
array('label'=>'Manage ProductsTypes','url'=>array('admin')),
);
?>

<h1>Create ProductsTypes</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>