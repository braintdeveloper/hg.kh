<?php
$this->breadcrumbs=array(
	'Products Types'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ProductsTypes','url'=>array('index')),
array('label'=>'Create ProductsTypes','url'=>array('create')),
array('label'=>'Update ProductsTypes','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ProductsTypes','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ProductsTypes','url'=>array('admin')),
);
?>

<h1>View ProductsTypes #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name_uk',
		'name_ru',
		'timecreate',
		'timeupdate',
),
)); ?>
