<?php
$this->breadcrumbs=array(
	'Questions Categories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List QuestionsCategory','url'=>array('index')),
	array('label'=>'Create QuestionsCategory','url'=>array('create')),
	array('label'=>'View QuestionsCategory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage QuestionsCategory','url'=>array('admin')),
	);
	?>

	<h1>Update QuestionsCategory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>