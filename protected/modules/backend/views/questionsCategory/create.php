<?php
$this->breadcrumbs=array(
	'Questions Categories'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List QuestionsCategory','url'=>array('index')),
array('label'=>'Manage QuestionsCategory','url'=>array('admin')),
);
?>

<h1>Create QuestionsCategory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>