<?php
$this->breadcrumbs=array(
	'Questions Categories',
);

$this->menu=array(
array('label'=>'Create QuestionsCategory','url'=>array('create')),
array('label'=>'Manage QuestionsCategory','url'=>array('admin')),
);
?>

<h1>Questions Categories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
