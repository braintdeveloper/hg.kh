<?php
$this->breadcrumbs=array(
	'Questions Categories'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List QuestionsCategory','url'=>array('index')),
array('label'=>'Create QuestionsCategory','url'=>array('create')),
array('label'=>'Update QuestionsCategory','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete QuestionsCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage QuestionsCategory','url'=>array('admin')),
);
?>

<h1>View QuestionsCategory #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'category_uk',
		'category_ru',
),
)); ?>
