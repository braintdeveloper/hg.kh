<?php

class PhotosController extends CBackend
{
	
	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		if(isset($_POST['Photos']))
		{
			//Upload picture
			$images = CUploadedFile::getInstancesByName('filesToUpload');
			
			// proceed if the images have been set
      if (isset($images) && count($images) > 0) { 
        // go through each uploaded image
        foreach ($images as $image => $pic) {
        	$model=new Photos();
					$model->attributes=$_POST['Photos'];
					$model->save();
					
					if($model->id) {
						$pic->saveAs($model->getPhotoPath($model->id));	
					}
        }
      }
			Yii::app()->user->setFlash('success', '<strong>Upload photos success</strong>');
		}
		
		$this->redirect(array($_POST['Photos']['redirect']));
	}
	
	public function actionUpdateAjax()
	{
		if(isset($_POST['pk']) && (int)$_POST['pk'] && isset($_POST['name']) && isset($_POST['value']))
		{
			$model = $this->loadModel($_POST['pk']);
			$model->setAttribute($_POST['name'], $_POST['value']);
			$model->save();
			return true;
		}		
		return false;		
	}		
	
	public function actionSetStatus($id, $value){
		if(isset($id) && isset($value)){
			if(
				$this->loadModel($id)->updateAll(
					array('status'=>$value),
					"id=$id"
				)
			){
				echo "Status updated";
			}
		}
	}
	
	public function actionMoveRow($id=false, $value=false, $type, $id_parent) {
		if((int)$id && ($value=='1' || $value=='-1') && $type && $id_parent){
			$model = new Photos();
			$currentRow = $model->findByPk($id);
			if($value=='1'){
				$nearRow = $model->find(array(
					'select'=>'*',
    			'condition'=>'sort>:sort and type=:type and id_parent=:id_parent',
    			'params'=>array(':sort'=>$currentRow->sort,':type'=>$type,':id_parent'=>$id_parent),
					'order'=>'sort asc'
				));				
				$return = 'Строка опущена.';
			} elseif($value=='-1'){
				$nearRow = $model->find(array(
					'select'=>'*',
    			'condition'=>'sort<:sort and type=:type and id_parent=:id_parent',
    			'params'=>array(':sort'=>$currentRow->sort,':type'=>$type,':id_parent'=>$id_parent),
					'order'=>'sort desc'
				));				
				$return = 'Строка поднята.';
			}
			if(isset($nearRow)){
				$model->updateByPk($nearRow->id, array('sort'=>$currentRow->sort));
				$model->updateByPk($currentRow->id, array('sort'=>$nearRow->sort));
				echo $return;	
			}			
		} else {
			$return = 'Ошибка, неверные параметры.';
		}
	}
	
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			@unlink($this->loadModel($id)->getPhotoPath($id));
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Photos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
}