<?php

	class UsersSamplingController extends CBackend
	{
		public $deliveryArray = array(
			'1'=>'New',
			'2'=>'In Progress',
			'3'=>'Delivered',
		);
		/**
		* Displays a particular model.
		* @param integer $id the ID of the model to be displayed
		*/
		public function actionView($id)
		{
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}

		/**
		* Creates a new model.
		* If creation is successful, the browser will be redirected to the 'view' page.
		*/
		public function actionCreate()
		{
			$model=new UsersSampling;

			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);

			if(isset($_POST['UsersSampling']))
			{
				$model->attributes=$_POST['UsersSampling'];
				if($model->save())
					$this->redirect(array('admin'));
			}

			$this->render('create',array(
				'model'=>$model,
			));
		}

		/**
		* Updates a particular model.
		* If update is successful, the browser will be redirected to the 'view' page.
		* @param integer $id the ID of the model to be updated
		*/
		public function actionUpdate($id)
		{
			$model=$this->loadModel($id);

			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);

			if(isset($_POST['UsersSampling']))
			{
				$model->attributes=$_POST['UsersSampling'];
				if($model->save())
					$this->redirect(array('update','id'=>$model->id));
			}

			$this->render('update',array(
				'model'=>$model,
			));
		}

		/**
		* Deletes a particular model.
		* If deletion is successful, the browser will be redirected to the 'admin' page.
		* @param integer $id the ID of the model to be deleted
		*/
		public function actionDelete($id)
		{
			if(Yii::app()->request->isPostRequest)
			{
				// we only allow deletion via POST request
				$this->loadModel($id)->delete();

				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
			else
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}

		/**
		* Lists all models.
		*/
		public function actionIndex()
		{
			$dataProvider=new CActiveDataProvider('UsersSampling');
			$this->render('index',array(
				'dataProvider'=>$dataProvider,
			));
		}

		/**
		* Manages all models.
		*/
		public function actionAdmin()
		{
			$model=new UsersSampling('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['UsersSampling']))
				$model->attributes=$_GET['UsersSampling'];

			$this->render('admin',array(
				'model'=>$model,
			));
		}

		/**
		* Returns the data model based on the primary key given in the GET variable.
		* If the data model is not found, an HTTP exception will be raised.
		* @param integer the ID of the model to be loaded
		*/
		public function loadModel($id)
		{
			$model=UsersSampling::model()->findByPk($id);
			if($model===null)
				throw new CHttpException(404,'The requested page does not exist.');
			return $model;
		}

		/**
		* Performs the AJAX validation.
		* @param CModel the model to be validated
		*/
		protected function performAjaxValidation($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='users-sampling-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
		
		
		public function actionExportCsv(){
			Yii::import('ext.ECSVExport.ECSVExport');
			
			$criteria = new CDbCriteria();
			$criteria->order = 'status_delivery ASC';
			$samplingList = UsersSampling::model()->findAll($criteria);
			
			$csv = new ECSVExport($samplingList);
			$csv->setDelimiter(';');
			$output = $csv->toCSV(); // returns string by default
			
			//var_dump($output);
			
			Yii::app()->getRequest()->sendFile('semplingUser.csv', $output, "text/csv");
			exit();			
		}
		
		public function actionImportCsv(){
			
			if(isset($_POST['UsersSampling'])) {
				if(!@$_FILES['UsersSampling']['name']['csvFile']) {
					Yii::app()->user->setFlash('error', 'Вы не загрузили файл');
				} else if(substr($_FILES['UsersSampling']['name']['csvFile'],-4) != '.csv') {
					Yii::app()->user->setFlash('error', 'Ошибка, неверный формат, загружайте только формат "csv" файла');
				} else {
					$row = 0;
					$count = 0;
					
					$filePath = $_FILES['UsersSampling']['tmp_name']['csvFile'];
					$handle = fopen($filePath, "r+");
					
					while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
						$num = count($data);

						$csvArray=array();
						for ($c=0; $c < $num; $c++) {
								$k = null;
								$k = ($c == 0)  ? 'id': $k;
								$k = ($c == 1)  ? 'fio': $k;
								$k = ($c == 10)  ? 'status_delivery': $k;
						    
						    $csvArray[$k] = trim($data[$c]);
						}
						
						if($row!=0){
							$user = UsersSampling::model()->find('id=:id AND fio=:fio', array('id'=>$csvArray['id'],'fio'=>$csvArray['fio']));
							if($user){
								if(UsersSampling::model()->updateByPk($user->id, array('status_delivery'=>$csvArray['status_delivery']))){
									$count++;
								}
							}
						}
						$row++;
					}
					Yii::app()->user->setFlash('success', 'Обновлено юзеров: '.$count.'.');
					
				}
			}
			
			/*if(!empty($_FILES['UsersSampling']['tmp_name']['csvFile']))
      {
        $file = CUploadedFile::getInstance($model,'csvFile');
        
        var_dump($file);die;
        
        
        $fp = fopen($file->tempName, 'r');
        if($fp) {
          //  $line = fgetcsv($fp, 1000, ",");
          //  print_r($line); exit;
          $first_time = true;
         	do {
          if ($first_time == true) {
              $first_time = false;
              continue;
          }
            var_dump($line['0']);
            var_dump($line['1']);
            //$model = new Registration;
            //$model->lastname  = $line[1];

            //$model->save();

          }while( ($line = fgetcsv($fp, 1000, ";")) != FALSE);
          
          $this->redirect('');
        }
        //    echo   $content = fread($fp, filesize($file->tempName));

      }*/
      
      $this->render('importCsv');
			
		}
		
		
	}
