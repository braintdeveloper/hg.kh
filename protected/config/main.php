<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// Define a path alias for the Bootstrap extension as it's used internally.
// In this example we assume that you unzipped the extension under protected/extensions.
//Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/yiibooster');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'HOLY GENERATION',
	'defaultController' => 'index',
	
	//'theme'=>'classic',
	'theme'=>'bootstrap',

	// preloading 'log' component
	'preload'=>array('log',),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
	),

	'modules'=>array(
		'backend'=>array(
     	'preload'=>array('bootstrap'),
     	'components'=>array(
      	'bootstrap'=>array(
        	'class' => 'ext.yiibooster.components.Bootstrap',
     		),
     	),
    ),
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths'=>array(
        'bootstrap.gii',
      ),
		),
		
	),
	
	//Multilang
	'sourceLanguage'=>'en',
  'language'=>'ru',
	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>'/site/login',
			'returnUrl' => array('/site/logout'),
		),
		'bootstrap'=>array(
    	'class' => 'ext.yiibooster.components.Bootstrap',
    	//'responsiveCss' => false,
    ),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'showScriptName'=>false, //Delete index.php from url
			'urlFormat'=>'path',
			'rules'=>array(				
				'<alias:contacts>'=>'textpages/view',
				'<alias:audio>'=>'textpages/view',
				'<alias:video>'=>'textpages/view',
				'<alias:team>'=>'textpages/view',
				'<alias:church>'=>'textpages/view',
				'page/<alias>'=>'textpages/view',
				
				'articles/search/<searcharticlesname:(.)+>'=>'articles/index',
				//'articles/search/'=>'articles/search',
				'articles'=>'articles/index',
				'articles/<alias:[a-z0-9_\-]+>'=>'articles/view',				
				
				/*'products'=>'products/index',
				'products/captcha'=>'products/captcha',
				'products/sampling'=>'products/sampling',
				'products/<alias:[a-z0-9_\-]+>'=>'products/view',*/
				
				
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			'class'=>'DLanguageUrlManager',
		),
		'request'=>array(
     	'class'=>'DLanguageHttpRequest',
     ),
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host='.DBHOST.';dbname='.DBNAME,
			'emulatePrepare' => true,
			'username' => DBUSER,
			'password' => DBPASSWORD,
			'charset' => 'utf8',
			'tablePrefix' => DBPREFIX,
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'index/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'mail@hg.kh.ua',
    
    'translatedLanguages'=>array(
        //'uk'=>'УКР',
        'ru'=>'РУС',
        //'en'=>'English',
        //'de'=>'Deutsch',        
    ),
    'defaultLanguage'=>'ru',		
    'adminSettings'=>'',
	),
);