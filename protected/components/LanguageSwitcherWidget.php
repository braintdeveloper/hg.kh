<?php
class LanguageSwitcherWidget extends CWidget
{
    public function run()
    {
        $currentUrl = ltrim(Yii::app()->request->url, '/');
        $links = array();
        foreach (DMultilangHelper::suffixList() as $suffix => $name){
        		$active='';
        		if($suffix=='_'.Yii::app()->getLanguage()) $active=' active';
        		if(empty($suffix) && Yii::app()->getLanguage()==Yii::app()->params->defaultLanguage) $active=' active';
        		
            $url = '/' . ($suffix ? trim($suffix, '_') . '/' : '') . $currentUrl;
            //$links[] = CHtml::tag('li', array('class'=>$suffix.$active), CHtml::link($name, $url));
            $links[] = CHtml::link($name, $url, array('class'=>$suffix.$active));
        }
        echo CHtml::tag('div', array('class'=>'lang'), implode("\n", $links)); 
        //echo CHtml::tag('ul', array(), implode("\n", $links)); 
    }
}