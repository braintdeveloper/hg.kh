<?php
class UtilityHtml {
	
	public static function getImagetitle($status) {
	    if ($status == 1 || strtolower($status) == 'yes') {
	        return 'Активный';
	    } else {
	        return 'Не активный';
	    }
	}
	 
	public static function getStatusImage($status) {
	    if ($status == 1 || strtolower($status) == 'yes') {
	        return Yii::app()->request->baseUrl . '/admin_style/images/tick.png';
	    } else {
	        return Yii::app()->request->baseUrl . '/admin_style/images/publish_x.png';
	    }
	}
	
	public function getPhotoUrl($folder=false, $id=false, $updateTime=false){
		if(is_file(Yii::getPathOfAlias('webroot.uploads.'.$folder.'.'.$id).'.jpg')){
			return Yii::app()->request->baseUrl.'/uploads/'.$folder.'/'.$id.'.jpg'.($updateTime ? '?'.$updateTime : '');
		}
		return false;
	}
		
	public function getPhotoPath($id=false){
		return Yii::getPathOfAlias('webroot.uploads.'.$folder.'.'.$id).'.jpg';	
	}
}