<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class CFrontend extends Controller
{
	
	public $layout='//layouts/index';
	
	public function init(){
    
    //Set jquery Core Sscript
    Yii::app()->clientScript->scriptMap = Yii::app()->clientScript->scriptMap + array('jquery.js'=>'/js/jquery-1.8.3.min.js');
    
    Yii::app()->params['adminSettings'] = CHtml::listData(Settings::model()->findAll(), 'id', 'value');

		parent::init();    
  }
}