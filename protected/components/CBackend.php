<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class CBackend extends Controller
{	
	public $statusArr = array('0'=>'Hide', '1'=>'Opened');
	
	public $layout='//layouts/column2';
	
	//Check User Role
	public function isAdmin(){
		$userModel = new User();
		return $userModel->isAccess(Yii::app()->user->id, 9);
	}	
	public function isModerator(){
		$userModel = new User();
		return $userModel->isAccess(Yii::app()->user->id, 7);
	}
	public function isEditor(){
		$userModel = new User();
		return $userModel->isAccess(Yii::app()->user->id, 5);
	}
	public function isUser(){
		$userModel = new User();
		return $userModel->isAccess(Yii::app()->user->id, 1);
	}	
	
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				//'actions'=>array('index', 'create', 'update', 'admin','delete'),
				'expression'=>'Yii::app()->controller->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
}