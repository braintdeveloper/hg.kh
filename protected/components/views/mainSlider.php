<?php
	//Yii::import('backend.models.ImageBehavior');
	//Yii::import('backend.models.Slider');
	$slidersObj = new Slider();
	$sliders = $slidersObj->findAll();
	
	$sliderClassStyle = array(
		1 => "flex-caption transparent light-font center",
		2 => "flex-caption transparent dark-font",
		3 => "flex-caption dark center",
	);
	$sliderButtonClassStyle = array(
		1 => "button medium white",
		2 => "button medium orange",
		3 => "button medium grey",
	);
?>	
<div class="fullwidth">
	<div class="flexslider" id="index-slider">			
		<ul class="slides">
			<?php 
				foreach($sliders as $slide) {
					$slide->image = $slidersObj->getPhotoUrl($slide->id, $slide->timeupdate);
			?>				
			<li>
				<a href="<?=$slide->linkbutton;?>"><img src="<?=$slide->image;?>" alt="<?=$slide->phrase1;?>" /></a>
				<div class="<?=$sliderClassStyle[$slide->type];?>">
					<div>
						<h2><span class="uppercase"><?=$slide->phrase1;?></span></h2>
						<p><span class="lowercase"><?=$slide->phrase2;?></span></p>
						<p><a href="<?=$slide->linkbutton;?>" class="<?=$sliderButtonClassStyle[$slide->type];?>"><?=$slide->buttonphrase;?></a></p>
					</div>
				</div>
			</li>
			<?php } ?>
		</ul><!--END UL SLIDES-->
	</div><!--END FLEXSLIDER-->		
</div><!--END FULLWIDTH-->