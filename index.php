<?php
defined('DBNAME') or define('DBNAME', 'hg');
defined('DBUSER') or define('DBUSER', 'root');
defined('DBPASSWORD') or define('DBPASSWORD', '');
defined('DBHOST') or define('DBHOST', 'localhost');
defined('DBPREFIX') or define('DBPREFIX', '');

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

//Counstants
defined('SALT') or define('SALT', 'hideMySalts14');
defined('TIME') or define('TIME', time());
defined('IMAGEPATH') or define('IMAGEPATH', __DIR__.'/uploads/');
//defined('IMAGEURL') or define('IMAGEURL', 's');

require_once($yii);
Yii::createWebApplication($config)->run();

